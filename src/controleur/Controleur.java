package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.*;
import vue.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.List;

public class Controleur implements Observer{
    private VueEtat vueEtat;
    private String modeActuel ;
    Carte carteCliquee ;
    String joueurCliqueeNom ;
    Joueur joueurCourant;
    int turnIndex ;
    private CarteMouseListener carteMouseListener;
    private ObservableCarteMouseListener observableCarteMouseListener;
    private Etat etat;

    public Controleur(VueEtat vueEtat) {
        this.vueEtat = vueEtat;
        modeActuel=null;
        carteCliquee = null;
        joueurCliqueeNom = null;
        etat=vueEtat.getEtat();

        turnIndex = 0;
        

        carteMouseListener = vueEtat.getCarteMouseListener();
        observableCarteMouseListener = new ObservableCarteMouseListener();      
        carteMouseListener.setObservable(observableCarteMouseListener);
        observableCarteMouseListener.addObserver(this);

        initBoutons();

    }

    private void initBoutons() {
        // Associer des écouteurs d'événements aux boutons
        vueEtat.getJouerCarteButton().addActionListener(new JouerCarteListener());
        vueEtat.getDefausserCarteButton().addActionListener(new DefausserCarteListener());
        vueEtat.getDonnerIndiceButton().addActionListener(new DonnerIndiceListener());
        vueEtat.getUtiliserIAButton().addActionListener(new UtiliserIAListener());
    }

    public void executerAction() {
        if(!etat.getPaquet().getCartes().isEmpty() && etat.getJetonsRouge() != 0){
            joueurCourant = etat.getJoueurs().get(turnIndex);
            boolean tourpasse=false;
            if(modeActuel!=null){
                switch (modeActuel) {
                    case "Jouer Carte":
                        if(joueurCourant.getNom().equals(joueurCliqueeNom)){
                            int indiceCarteAJouer = joueurCourant.getIndiceCarte(carteCliquee);
                            if( indiceCarteAJouer != -1){
                                Carte carteAJouer = joueurCourant.jouerCarte(indiceCarteAJouer);
                                boolean passORnot = etat.getFeux().ajouterCarte(carteAJouer);
                                if (!passORnot) {
                                    JOptionPane.showMessageDialog(null, " !! Vous avez joué une mauvaise Carte !! ", "Erreur", JOptionPane.ERROR_MESSAGE); 
                                    etat.setJetonsRouge();
                                    etat.defausserCarte(carteAJouer);
                                }
                                joueurCourant.piocherCarte(etat.getPaquet().piocher());
                                tourpasse=true;
                            }else {
                                JOptionPane.showMessageDialog(null, " !! Carte non selectionnée !! ", "Erreur", JOptionPane.ERROR_MESSAGE); 
                                tourpasse=false;
                            }
                        }else {
                            JOptionPane.showMessageDialog(null, " !! Selectionner une carte de votre joueur !! ", "Erreur", JOptionPane.ERROR_MESSAGE); 
                            tourpasse=false;
                        }
                        break;
                    case "Defausser Carte":
                        if(joueurCourant.getNom().equals(joueurCliqueeNom)){
                            int indiceCarteAdefausser = joueurCourant.getIndiceCarte(carteCliquee);
                            if(indiceCarteAdefausser!=-1){
                                Carte carteAdefausser = joueurCourant.defausserCarte(indiceCarteAdefausser);
                                etat.defausserCarte(carteAdefausser);
                                etat.setJetonsBleu(1);
                                joueurCourant.piocherCarte(etat.getPaquet().piocher()); 
                                tourpasse=true;
                            }else {
                                JOptionPane.showMessageDialog(null, " !! Carte non selectionnée  !! ", "Erreur", JOptionPane.ERROR_MESSAGE); 
                                tourpasse=false;
                            }
                        }else {
                            JOptionPane.showMessageDialog(null, " !! Selectionner une carte de votre joueur  !! ", "Erreur", JOptionPane.ERROR_MESSAGE); 
                            tourpasse=false;
                        }
                        break;
                    case "Donner Indice":
                        if(!joueurCourant.getNom().equals(joueurCliqueeNom)){
                           if(etat.getJetonsBleu()>0){
                                Joueur autreJoueur = etat.getJoueurParNom(joueurCliqueeNom);
                                String indice =  getIndice(autreJoueur,carteCliquee);
                                if(indice!=null){
                                    etat.ajouterIndice(autreJoueur, indice);
                                    etat.setJetonsBleu(-1);
                                    tourpasse=true;
                                    }
                                else {
                                    System.out.println("indice null");
                                    tourpasse=false;
                                }
                           }else{
                                JOptionPane.showMessageDialog(null, " !! PAS DE JETON BLEU , FAUT CHOISIR UN AUTRE MODE DE JEU !! ", "Erreur", JOptionPane.ERROR_MESSAGE); 
                           }
                        }else{
                            JOptionPane.showMessageDialog(null, " !! Selectionner une carte d'un autre joueur !! ", "Erreur", JOptionPane.ERROR_MESSAGE); 
                        }
                        break;
                    case "Utiliser IA":
                        // Exécuter l'action correspondant au mode "Utiliser IA"
                        jouerAvecAI(etat,joueurCourant);
                        tourpasse=true;
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "choisir le mode de jeu !", "Fin du jeu", JOptionPane.INFORMATION_MESSAGE);
                        break;
                }
                modeActuel=null;
            }
            else{
                JOptionPane.showMessageDialog(null, " !! choisir le mode de jeu !! ", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
            if(tourpasse)
                jouerSansJoueurPrincipal(etat);
        }else{
            String message = "Fin du jeu !" + "\nLe score final est : " + etat.getFeux().score() ;
            JOptionPane.showMessageDialog(null, message, "Fin du jeu", JOptionPane.INFORMATION_MESSAGE);
            }
    }


    public void jouerAvecAI(Etat etat, Joueur joueur) {
        if (!etat.getPaquet().getCartes().isEmpty() && etat.getJetonsRouge() != 0) {
            // Affichage de la boîte de dialogue pour sélectionner la stratégie
            Object[] options = {"Statistique", "Probabiliste","Observatrice", "Aléatoire"};
            int choix = JOptionPane.showOptionDialog(null, "Pour le joueur " + joueur.getNom() +" Choisissez une stratégie :", "Sélection de la stratégie",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

            // Création de la stratégie en fonction du choix de l'utilisateur
            Strategie strategie = null;
            switch (choix) {
                case 0:
                    strategie = new StrategieStatistique();
                    break;
                case 1:
                    strategie = new StrategieProbabiliste(0.8f);
                    break;
                case 2:
                    strategie = new StrategieObservatrice();
                    break;
                case 3:
                    strategie = new StrategieAleatoire();
                    break;
                default:
                    // Stratégie par défaut au cas où l'utilisateur ferme la boîte de dialogue sans choisir
                    strategie = new StrategieStatistique();
                    JOptionPane.showMessageDialog(null, " !! On a choisi Strategie Statistique par default !! ", "Erreur", JOptionPane.ERROR_MESSAGE);

            }

            // Exécution de la stratégie sélectionnée
            Action action = strategie.actionAfaire(joueur, etat);
            JouerSelonAction(action, etat);
        } else {
            String message = "Fin du jeu !\nLe score final est : " + etat.getFeux().score();
            JOptionPane.showMessageDialog(null, message, "Fin du jeu", JOptionPane.INFORMATION_MESSAGE);
        }
    }



    public void jouerSansJoueurPrincipal(Etat etat){
        if(!etat.getPaquet().getCartes().isEmpty() && etat.getJetonsRouge() != 0){
            turnIndex = (turnIndex + 1) % etat.getJoueurs().size();
            joueurCourant = etat.getJoueurs().get(turnIndex);
            while(!joueurCourant.getNom().equals(vueEtat.getMonJoueur().getNom()) ){
                if(etat.getPaquet().getCartes().isEmpty() || etat.getJetonsRouge() == 0){
                    String message = "Fin du jeu !" + "\nLe score final est : " + etat.getFeux().score() ;
                    JOptionPane.showMessageDialog(null, message, "Fin du jeu", JOptionPane.INFORMATION_MESSAGE);                    
                    break;
                }
                Object[] options = {"Statistique","Probabiliste","Observatrice", "Aléatoire"};
                int choix = JOptionPane.showOptionDialog(null, "Pour le joueur " + joueurCourant.getNom() +" Choisissez une stratégie :", "Sélection de la stratégie",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

                // Création de la stratégie en fonction du choix de l'utilisateur
                Strategie strategie = null;
                switch (choix) {
                    case 0:
                        strategie = new StrategieStatistique();
                        break;
                    case 1:
                        strategie = new StrategieProbabiliste(0.8f);
                        break;
                    case 2:
                        strategie = new StrategieObservatrice();
                        break;
                    case 3:
                        strategie = new StrategieAleatoire();
                        break;
                    default:
                        // Stratégie par défaut au cas où l'utilisateur ferme la boîte de dialogue sans choisir
                        strategie = new StrategieStatistique();
                        JOptionPane.showMessageDialog(null, " !! On a choisi Strategie Statistique par default !! ", "Erreur", JOptionPane.ERROR_MESSAGE);

                }
                Action action = strategie.actionAfaire(joueurCourant, etat);
                JouerSelonAction(action,etat);
                // Mettre à jour l'indice de tour pour le prochain joueur
                turnIndex = (turnIndex + 1) % etat.getJoueurs().size();
                joueurCourant = etat.getJoueurs().get(turnIndex);

            }
        }else {
            String message = "Fin du jeu !" + "\nLe score final est : " + etat.getFeux().score() ;
            JOptionPane.showMessageDialog(null, message, "Fin du jeu", JOptionPane.INFORMATION_MESSAGE);
        }
        
       

    }

    public void JouerSelonAction(Action action ,Etat etat){
         if (action != null) {
                switch (action.getType()) {
                    case Action.TYPE_JOUEUR_INDICE:
                        // Donner un indice                        
                        Joueur autreJoueur = etat.getJoueurParNom(action.getJoueur());
                        String indice = isInteger(action.getIndice())? donnerIndiceValeur(autreJoueur,Integer.parseInt(action.getIndice())): donnerIndiceCouleur(autreJoueur,action.getIndice());
                        etat.ajouterIndice(autreJoueur, indice);
                        etat.setJetonsBleu(-1);
                        break;
                    case Action.TYPE_CARTE_JOUER:
                        // Jouer une carte
                        int indiceCarteAJouer = action.getIndiceCarteJoueur();
                        Carte carteAJouer = joueurCourant.jouerCarte(indiceCarteAJouer);
                        boolean passORnot = etat.getFeux().ajouterCarte(carteAJouer);
                        if (!passORnot) {
                            JOptionPane.showMessageDialog(null, " !! Vous avez joué une mauvaise Carte !! ", "Erreur", JOptionPane.ERROR_MESSAGE);
                            etat.setJetonsRouge();
                            etat.defausserCarte(carteAJouer);
                        }
                        joueurCourant.piocherCarte(etat.getPaquet().piocher());
                        break;
                    case Action.TYPE_CARTE_DEFAUSSER:
                        // Défausser une carte
                        int indiceCarteAdefausser = action.getIndiceCarteDefausser();
                        Carte carteAdefausser = joueurCourant.defausserCarte(indiceCarteAdefausser);
                        etat.defausserCarte(carteAdefausser);
                        etat.setJetonsBleu(1);
                        joueurCourant.piocherCarte(etat.getPaquet().piocher());
                        break;
                }
            }
    }


    public String getIndice(Joueur autreJoueur, Carte carteCliquee) {
        String indice;
        int indicecarte = autreJoueur.getIndiceCarte(carteCliquee);
        if (indicecarte != -1) {
            Carte carteAReveler = autreJoueur.getMain().get(indicecarte);
            if (carteAReveler.estReveleeCouleur() && carteAReveler.estReveleeValeur()) {
                JOptionPane.showMessageDialog(null, " !! Carte déjà révélée !! ", "Erreur", JOptionPane.ERROR_MESSAGE);
                return null;
            }
            if (!carteAReveler.estReveleeCouleur() && !carteAReveler.estReveleeValeur()) {
                // Afficher une fenêtre de choix pour la couleur ou la valeur
                int choix = JOptionPane.showOptionDialog(null, "Choisissez l'indice à révéler:", "Choix de l'indice",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                        new Object[] { "Couleur", "Valeur" }, null);
                if (choix == JOptionPane.CLOSED_OPTION) {
                    return null; // L'utilisateur a annulé
                } else if (choix == 0) {
                    return donnerIndiceCouleur(autreJoueur, carteAReveler.getCouleur());
                } else {
                    return donnerIndiceValeur(autreJoueur, carteAReveler.getValeur());
                }
            }

            if (!carteAReveler.estReveleeCouleur() && carteAReveler.estReveleeValeur()) {
                return donnerIndiceCouleur(autreJoueur, carteAReveler.getCouleur());
            }

            if (carteAReveler.estReveleeCouleur() && !carteAReveler.estReveleeValeur()) {
                return donnerIndiceValeur(autreJoueur, carteAReveler.getValeur());
            }
        } else {
            System.out.println(" !! Carte inexistante dans la main du joueur : " + autreJoueur.getNom() + " !!");
        }

        return null;
    }


    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ObservableCarteMouseListener) {
            // Réagir aux changements dans CarteMouseListener
            ObservableCarteMouseListener observable = (ObservableCarteMouseListener) o;
            carteCliquee = observable.getCarteCliquee();
            joueurCliqueeNom = observable.getJoueurNom();
            executerAction();
        }
    }
    // Implémenter des classes internes pour chaque action
    private class JouerCarteListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            modeActuel = "Jouer Carte";
            
        }
    }

    private class DefausserCarteListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
        modeActuel = "Defausser Carte";       
        }
    }

    private class DonnerIndiceListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            modeActuel = "Donner Indice";
        }
    }

    private class UtiliserIAListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            modeActuel = "Utiliser IA";
            executerAction();
        }
    }
     /**
     * Donne des indices sur les cartes d'une certaine couleur dans la main d'un autre joueur.
     * @param autreJoueur Le joueur dont on veut connaître les cartes.
     * @param couleur La couleur des cartes pour lesquelles on veut des indices.
     * @return Une chaîne décrivant le nombre et les positions des cartes de la couleur spécifiée.
     */
    public String donnerIndiceCouleur(Joueur autreJoueur, String couleur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices = "";
        int compt = 0;
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getCouleur().equals(couleur)) {
                    carte.revelerCouleur(); // Révèle la couleur de la carte
                    compt++; // Incrémente le compteur de cartes trouvées
                    indices += " " + mainAutreJoueur.indexOf(carte); // Ajoute l'indice de la carte aux indices
                }
            }
        }
        if (compt != 0)
            return " a " + compt + " cartes de couleur " + couleur + " à l'indice " + indices;
        return null; // Retourne null si aucune carte correspondante n'est trouvée
    }

    /**
     * Donne des indices sur les cartes d'une certaine valeur dans la main d'un autre joueur.
     * @param autreJoueur Le joueur dont on veut connaître les cartes.
     * @param valeur La valeur des cartes pour lesquelles on veut des indices.
     * @return Une chaîne décrivant le nombre et les positions des cartes de la valeur spécifiée.
     */
    public String donnerIndiceValeur(Joueur autreJoueur, int valeur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices = "";
        int compt = 0;
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getValeur() == valeur) {
                    carte.revelerValeur(); // Révèle la valeur de la carte
                    compt++; // Incrémente le compteur de cartes trouvées
                    indices += " " + mainAutreJoueur.indexOf(carte); // Ajoute l'indice de la carte aux indices
                }
            }
        }
        if (compt != 0)
            return " a " + compt + " cartes de valeur " + valeur + " à l'indice " + indices;
        return null; // Retourne null si aucune carte correspondante n'est trouvée
    }

     /**
     * Cette méthode vérifie si une chaîne de caractères peut être convertie en un entier.
     * @param str La chaîne de caractères à vérifier.
     * @return true si la chaîne peut être convertie en entier, false autrement.
     */
    public static boolean isInteger(String str) {
        if (str == null) {
            return false; // Retourne false si la chaîne est null
        }
        try {
            int num = Integer.parseInt(str); // Essaie de convertir la chaîne en entier
            return true; // Retourne true si la conversion réussit
        } catch (NumberFormatException e) {
            return false; // Retourne false si une NumberFormatException est levée
        }
    }
}