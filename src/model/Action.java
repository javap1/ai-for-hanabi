package model;


public class Action {
    public static final int TYPE_JOUEUR_INDICE = 1;
    public static final int TYPE_CARTE_JOUER = 2;
    public static final int TYPE_CARTE_DEFAUSSER = 3;

    private int type;
    private String joueurNom;
    private String indice;
    private int indiceCarteJouer;
    private int indiceCarteDefausser;

    // Constructeur pour le type 1
    public Action(String joueurNom, String indice) {
        this.type = TYPE_JOUEUR_INDICE;
        this.joueurNom = joueurNom;
        this.indice = indice;
    }

    // Constructeur pour le type 2
    public Action(int indiceCarteJouer) {
        this.type = TYPE_CARTE_JOUER;
        this.indiceCarteJouer = indiceCarteJouer;
    }

    // Constructeur pour le type 3
    public Action(int type, int indiceCarteDefausser) {
        this.type = TYPE_CARTE_DEFAUSSER;
        this.indiceCarteDefausser = indiceCarteDefausser;
    }

    // Méthodes d'accès pour le type d'action

    public int getType() {
        return type;
    }
    // Méthodes d'accès pour le type 1
    public String getJoueur() {
        if (type == TYPE_JOUEUR_INDICE) {
            // Vous devez avoir une classe Joueur avec un constructeur approprié
            return joueurNom;
        } else {
            throw new IllegalStateException("Cette action n'est pas de type JOUEUR_INDICE.");
        }
    }

    public String getIndice() {
        if (type == TYPE_JOUEUR_INDICE) {
            return indice;
        } else {
            throw new IllegalStateException("Cette action n'est pas de type JOUEUR_INDICE.");
        }
    }

    // Méthodes d'accès pour le type 2
    public int getIndiceCarteJoueur() {
        if (type == TYPE_CARTE_JOUER) {
            return indiceCarteJouer;
        } else {
            throw new IllegalStateException("Cette action n'est pas de type CARTE_JOUEUR.");
        }
    }

    // Méthodes d'accès pour le type 3

    public int getIndiceCarteDefausser() {
        if (type == TYPE_CARTE_DEFAUSSER) {
            return indiceCarteDefausser;
        } else {
            throw new IllegalStateException("Cette action n'est pas de type CARTE_DEFAUSSER.");
        }
    }


}