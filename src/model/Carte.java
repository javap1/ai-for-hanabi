package model;

import java.util.ArrayList;
import java.util.List;

public class Carte implements Observable, Cloneable {
    private String couleur;
    private int valeur;
    private boolean reveleeCouleur;
    private boolean reveleeValeur;
    private boolean changed = false;
    private List<Observer> observers = new ArrayList<>();

    public Carte(String couleur, int valeur) {
        this.couleur = couleur;
        this.valeur = valeur;
        this.reveleeCouleur = false;
        this.reveleeValeur = false;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            // Gérer l'exception de clonage
            return null;
        }
    }

    public String getCouleur() {
        return couleur;
    }

    public int getValeur() {
        return valeur;
    }

    public boolean estReveleeCouleur() {
        return reveleeCouleur;
    }

    public void revelerCouleur() {
        this.reveleeCouleur = true;
        setChanged(); // Marquer l'état de la carte comme modifié
        notifyObservers(); // Notifier les observateurs du changement
    }

    public boolean estReveleeValeur() {
        return reveleeValeur;
    }

    public void revelerValeur() {
        this.reveleeValeur = true;
        setChanged(); // Marquer l'état de la carte comme modifié
        notifyObservers(); // Notifier les observateurs du changement
    }

    @Override
    public String toString() {
        return "Carte{" +
                "couleur='" + couleur + '\'' +
                ", valeur=" + valeur +
                ", reveleeValeur=" + reveleeValeur +
                ", reveleeCouleur=" + reveleeCouleur +
                '}';
    }

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        if (!changed) return;

        for (Observer observer : observers) {
            observer.update(this, null);
        }

        clearChanged();
    }

    

    @Override
    public void setChanged() {
        changed = true;
    }

    private void clearChanged() {
        changed = false;
    }
}
