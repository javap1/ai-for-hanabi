package model;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        List<Strategie> strategies = new ArrayList<>();
        strategies.add(new StrategieProbabiliste(0.8f));
        strategies.add(new StrategieProbabiliste(0.8f));
        strategies.add(new StrategieProbabiliste(0.8f));

        Jeu jeu = new Jeu(3);
        System.out.println(jeu.getEtat());
        Etat etatCopie = jeu.getEtat().copy();
        System.out.println(etatCopie);        // Partie avec l'état normal
        System.out.println("Partie avec l'état normal :");
        jeu.jouer(strategies);

        // Créez une copie de l'état actuel
        

        // Modifiez les stratégies pour utiliser la stratégie statistique
        List<Strategie> strategiesCopie = new ArrayList<>();
        strategiesCopie.add(new StrategieStatistique());
        strategiesCopie.add(new StrategieStatistique());
        strategiesCopie.add(new StrategieStatistique());

        // Remplacez l'état actuel par l'état copié
        Jeu jeu1 = new Jeu(3);
        jeu1.remplacerEtat(etatCopie);

        // Lancez la deuxième partie avec l'état copié et les nouvelles stratégies
        System.out.println("\nPartie avec l'état copié et les nouvelles stratégies :");
        jeu1.jouer(strategies);
    }
}
