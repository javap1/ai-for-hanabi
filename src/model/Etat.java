package model;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Etat implements Observable {

    private List<Joueur> joueurs;
    private Paquet paquet;
    private FeuArtifice feux;
    private Map<Joueur, List<String>> indicesReçus;
    private List<Carte> cartesDefausses;
    private int jetonsBleu;
    private int jetonsRouge;
    private boolean changed = false;
    private List<Observer> observers = new ArrayList<>();

    public Etat(List<Joueur> joueurs) {
        this.joueurs = joueurs;
        paquet = new Paquet();
        feux = new FeuArtifice();
        indicesReçus = new HashMap<>();
        cartesDefausses = new ArrayList<>();
        jetonsBleu = 8;
        jetonsRouge = 3;

        for (Joueur joueur : joueurs) {
            indicesReçus.put(joueur, new ArrayList<>());
        }
        
        this.initialiserMains();
    }

    //initialiser la main des joueurs
    public void initialiserMains() {
        int k = 5;
        if (joueurs.size() >= 4)
            k = 4;
        for (int i = 0; i < k; i++) {
            for (Joueur j : this.joueurs) {
                Carte c = this.paquet.piocher();
                if (c != null)
                    j.piocherCarte(c);
            }
        }
    }
    // Méthodes pour ajouter, supprimer, etc. des cartes, joueurs, etc.

    public void defausserCarte(Carte carte) {
            cartesDefausses.add(carte);
            setChanged();
            notifyObservers();
    }

    public void ajouterJoueur(Joueur joueur) {
        joueurs.add(joueur);
    }

    public void supprimerJoueur(Joueur joueur) {
        joueurs.remove(joueur);
    }

    public void ajouterIndice(Joueur joueur, String indice) {
        if (!containsIndice(joueur, indice)) {
            List<String> indicesJoueur = indicesReçus.get(joueur);
            if (indicesJoueur == null) {
                indicesJoueur = new ArrayList<>();
            }
            indicesJoueur.add(indice);
            indicesReçus.put(joueur, indicesJoueur);
        } else {
            System.out.println("L'indice existe déjà pour ce joueur.");
        }
}

    
    public void supprimerIndice(Joueur joueur, String indice) {
        List<String> indicesJoueur = indicesReçus.get(joueur);
        if (indicesJoueur != null) {
            indicesJoueur.remove(indice);
        }
    }
    
    public boolean containsIndice(Joueur joueur, String indice) {
    List<String> indicesJoueur = indicesReçus.get(joueur);
    return indicesJoueur != null && indicesJoueur.contains(indice);
}


    // Méthodes pour obtenir les informations de l'état

    public List<Joueur> getJoueurs() {
        return joueurs;
    }

    public Paquet getPaquet() {
        return this.paquet;
    }

    public FeuArtifice getFeux() {
        return feux;
    }

    public Map<Joueur, List<String>> getIndicesReçus() {
        return indicesReçus;
    }

    public List<Carte> getCartesDefausses() {
        return cartesDefausses;
    }

    public int getJetonsBleu() {
        return jetonsBleu;
    }

    public int getJetonsRouge() {
        return jetonsRouge;
    }

    // Méthodes pour modifier les attributs de l'état

    public void setJetonsBleu(int change) {
        if(change==1 && this.jetonsBleu<8)
         this.jetonsBleu++;
        if(change==-1 && this.jetonsBleu>0)
         this.jetonsBleu--;
         setChanged();
         notifyObservers();
    }

    public void setJetonsRouge() {
        this.jetonsRouge--;
        setChanged();
        notifyObservers();
    }



    public Joueur getJoueurParNom(String nomJoueur) {
        for (Joueur joueur : joueurs) {
            if (joueur.getNom().equals(nomJoueur)) {
                return joueur;
            }
        }
        return null; // Retourne null si aucun joueur avec le nom spécifié n'est trouvé
    }
    
    public boolean defausserContains(int valeur, String couleur) {
        for (Carte carte : cartesDefausses) {
            if (carte.getValeur() == valeur && carte.getCouleur().equals(couleur)) {
                return true; // La carte avec la valeur et la couleur spécifiées est présente dans les cartes défaussées
            }
        }
        return false; // La carte n'est pas présente dans les cartes défaussées
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        // Informations sur les joueurs
        sb.append("Joueurs:\n");
        for (Joueur joueur : joueurs) {
            sb.append(" - ").append(joueur.getNom()).append("\n");
            sb.append(" - ").append(joueur.toString()).append("\n");

        }

        // Informations sur le paquet
        sb.append("Paquet: ").append(paquet.taille()).append("\n");
        sb.append("Paquet: ").append(paquet.getCartes()).append("\n");

        // Informations sur les feux d'artifice
        sb.append("Feux d'artifice: ").append(feux).append("\n");

        // Informations sur les indices reçus
        sb.append("Indices reçus:\n");
        for (Map.Entry<Joueur, List<String>> entry : indicesReçus.entrySet()) {
            Joueur joueur = entry.getKey();
            List<String> indices = entry.getValue();
            sb.append(" - ").append(joueur.getNom()).append(": ").append(indices).append("\n");
        }

        // Informations sur les cartes défaussées
        sb.append("Cartes défaussées: ").append(cartesDefausses).append("\n");

        // Informations sur les jetons
        sb.append("Jetons Bleu: ").append(jetonsBleu).append("\n");
        sb.append("Jetons Rouge: ").append(jetonsRouge).append("\n");

        return sb.toString();
    }
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        if (!changed) return;

        for (Observer observer : observers) {
            observer.update(this, null);
        }

        clearChanged();
    }

    

    @Override
    public void setChanged() {
        changed = true;
    }

    private void clearChanged() {
        changed = false;
    }
    
     public Etat copy() {
        List<Joueur> joueursCopie = new ArrayList<>();
        

        Etat copie = new Etat(joueursCopie);
        copie.getJoueurs().clear();
        for (Joueur joueur : this.joueurs) {
            copie.getJoueurs().add(joueur.copy()); // Assurez-vous que Joueur a une méthode copy()
        }
 
        copie.paquet = this.paquet.copy(); // Assurez-vous que Paquet a une méthode copy()
        copie.feux = new FeuArtifice(); // Utilisez la bonne casse pour le nom de classe
        copie.cartesDefausses = new ArrayList<>(); // Copier les cartes défaussées
        copie.jetonsBleu = 8;
        copie.jetonsRouge = 3;

        return copie;
    }
}















