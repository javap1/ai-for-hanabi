package model;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class FeuArtifice implements Observable{
    private Map<String, List<Carte>> piles;
    private boolean changed = false;
    private List<Observer> observers = new ArrayList<>();

    public FeuArtifice() {
        this.piles = new HashMap<>();
        initialiserPiles();
    }

    private void initialiserPiles() {
        piles.put("Rouge", new ArrayList<>());
        piles.put("Bleu", new ArrayList<>());
        piles.put("Vert", new ArrayList<>());
        piles.put("Jaune", new ArrayList<>());
        piles.put("Blanc", new ArrayList<>());
    }

    public boolean ajouterCarte(Carte carte) {
        String couleur = carte.getCouleur();
        int valeur = carte.getValeur();
        List<Carte> pile = piles.get(couleur);

        if (pile.isEmpty()) {
            // Si la pile est vide, la carte doit avoir une valeur de 1
            if (valeur == 1) {
                pile.add(carte);
                setChanged(); // Indiquer que l'état a changé
                notifyObservers(); // Notifier les observateurs 
                return true;
            }
        } else {
            // Si la pile n'est pas vide, la carte doit avoir une valeur immédiatement supérieure à celle du sommet de la pile
            Carte carteSommet = pile.get(pile.size() - 1);
            if (carteSommet.getValeur() + 1 == valeur) {
                pile.add(carte);
                setChanged(); // Indiquer que l'état a changé
                notifyObservers(); // Notifier les observateurs 
                return true;
            }
        }
        return false;
}
    public boolean estCarteSuivante(String couleur, int valeur) {
        List<Carte> pile = piles.get(couleur);

        if (pile.isEmpty()) {
            // Si la pile est vide, la carte doit avoir une valeur de 1
            return valeur == 1;
        } else {
            // Si la pile n'est pas vide, la carte doit avoir une valeur immédiatement supérieure à celle du sommet de la pile
            Carte carteSommet = pile.get(pile.size() - 1);
            return carteSommet.getValeur() + 1 == valeur;
        }
    }

    public boolean estCarteJouable(Carte carte) {
        return estCarteSuivante(carte.getCouleur(), carte.getValeur());
    }

    public boolean estCarteJouable(int valeurCarte) {
        int cmpt = 0; // Initialisation d'un compteur

        // Parcours de toutes les piles de cartes
        for (List<Carte> pile : piles.values()) {
            if (pile.size() == valeurCarte-1) {
                cmpt++; // Incrémente le compteur si la taille de la pile est supérieure ou égale à valeurCarte
            }
        }

        // La carte est considérée inutile si toutes les piles ont une taille supérieure ou égale à valeurCarte
        return cmpt == 5;
    }    

    public boolean estCarteInutile(Carte carte) {
        int valeurCarte = carte.getValeur();
        String couleurCarte = carte.getCouleur();

        // Vérifier si une carte de valeur supérieure est présente dans la pile de feux
        if (getPile(couleurCarte).size() >= valeurCarte) {
            return true; // La carte est inutile si une carte de valeur égale ou supérieure est déjà dans la pile
        }
        return false;
    }
 
    /**
     * Vérifie si une carte est considérée comme inutile en fonction du nombre de piles
     * ayant une taille supérieure ou égale à la valeur de la carte.
     *
     * @param valeurCarte La valeur de la carte à évaluer.
     * @return true si toutes les piles ont une taille supérieure ou égale à valeurCarte, sinon false.
     */
    public boolean estCarteInutile(Integer valeurCarte) {
        int cmpt = 0; // Initialisation d'un compteur

        // Parcours de toutes les piles de cartes
        for (List<Carte> pile : piles.values()) {
            if (pile.size() >= valeurCarte) {
                cmpt++; // Incrémente le compteur si la taille de la pile est supérieure ou égale à valeurCarte
            }
        }

        // La carte est considérée inutile si toutes les piles ont une taille supérieure ou égale à valeurCarte
        return cmpt == 5;
    }

         
    public boolean estCarteJouablePlusTard(Carte carte) {
        int valeurCarte = carte.getValeur();
        
        // Vérifier si une carte de valeur inférieure ou egal est présente dans la pile de feux
        if (getPile(carte.getCouleur()).size() < valeurCarte-1 ) {
            return true;
        }
        // Ajoutez d'autres conditions spécifiques à votre jeu si nécessaire

        return false;
    }
    
    public boolean existePileAvecSize(int size) {
        for (List<Carte> pile : piles.values()) {
            if (pile.size() == size) {
                return true;
            }
        }
        return false;
    }

    
    public List<Carte> getPile(String couleur) {
        return piles.getOrDefault(couleur, new ArrayList<>());
    }
    
    public  Map<String, List<Carte>> getPiles() {
        return this.piles ;
    }
    
    public int score(){
        int score =0;
       for (List<Carte> pile : piles.values()) {
                score+=pile.size();
            
        }
        return score;
    }



    @Override
    public String toString() {
        return "FeuArtifice{" +
                "piles=" + piles +
                '}';
    }
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        if (!changed) return;

        for (Observer observer : observers) {
            observer.update(this, null);
        }

        clearChanged();
    }

    

    @Override
    public void setChanged() {
        changed = true;
    }

    private void clearChanged() {
        changed = false;
    }
}

