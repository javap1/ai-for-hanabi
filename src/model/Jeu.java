package model;

import java.util.ArrayList;
import java.util.List;


import java.util.Random;

public class Jeu {

    private final List<Joueur> joueurs;
    private Etat etat;
    private int score;
    public Jeu(int nbjoueurs) {
        joueurs = new ArrayList<>();
        Random random = new Random();
        
        // Create players
        for (int i = 1; i <= nbjoueurs; i++) {
            joueurs.add(new Joueur("" + i));
        }
        
        this.etat = new Etat(joueurs);
        this.score=0;
    }

    public void setEtatAzero(){
         int nbjoueurs=joueurs.size();
         joueurs.clear();

        // Recréer les joueurs
        for (int i = 1; i <= nbjoueurs; i++) {
            joueurs.add(new Joueur("" + i));
        }

         this.etat= new Etat(joueurs);
         this.score=0;
    }

    public void jouer( List<Strategie> strategies) {
        int choix;
        Random random = new Random();
        int turnIndex = 0;
        Strategie strategie ;
         
        // Boucle principale du jeu
        while (!etat.getPaquet().getCartes().isEmpty() && etat.getJetonsRouge() != 0) {
            System.out.println(etat.toString());
            Joueur joueurCourant = etat.getJoueurs().get(turnIndex);
            System.out.println("C'est le tour du Joueur " + joueurCourant.getNom());

            System.out.println("Vous avez trois choix : ");
            System.out.println("1- Donner un indice");
            System.out.println("2- Jouer une carte");
            System.out.println("3- Défausser une carte");
            

            strategie = strategies.get(turnIndex % strategies.size());
            System.out.println("" + strategie.toString());
            Action action = strategie.actionAfaire(joueurCourant, etat);



            if (action != null) {
                switch (action.getType()) {
                    case Action.TYPE_JOUEUR_INDICE:
                        // Donner un indice                        
                        Joueur autreJoueur = etat.getJoueurParNom(action.getJoueur());
                        String indice = isInteger(action.getIndice())? donnerIndiceValeur(autreJoueur,Integer.parseInt(action.getIndice())): donnerIndiceCouleur(autreJoueur,action.getIndice());
                        System.out.printf("Joueur %s : %s%n", autreJoueur.getNom(), indice);
                        etat.ajouterIndice(autreJoueur, indice);
                        etat.setJetonsBleu(-1);
                        break;
                    case Action.TYPE_CARTE_JOUER:
                        // Jouer une carte
                        int indiceCarteAJouer = action.getIndiceCarteJoueur();
                        Carte carteAJouer = joueurCourant.jouerCarte(indiceCarteAJouer);
                        System.out.println("carte jouée : " + carteAJouer.toString() );
                        boolean passORnot = etat.getFeux().ajouterCarte(carteAJouer);
                        if (!passORnot) {
                            System.out.println("Vous avez joué une mauvaise Carte");
                            etat.setJetonsRouge();
                            etat.defausserCarte(carteAJouer);
                        }
                        joueurCourant.piocherCarte(etat.getPaquet().piocher());
                        break;
                    case Action.TYPE_CARTE_DEFAUSSER:
                        // Défausser une carte
                        int indiceCarteAdefausser = action.getIndiceCarteDefausser();
                        Carte carteAdefausser = joueurCourant.defausserCarte(indiceCarteAdefausser);
                        etat.defausserCarte(carteAdefausser);
                        System.out.println("carte defaussée : " + carteAdefausser.toString() );
                        etat.setJetonsBleu(1);
                        joueurCourant.piocherCarte(etat.getPaquet().piocher());
                        break;
                }
            }

            // Mettre à jour l'indice de tour pour le prochain joueur
            turnIndex = (turnIndex + 1) % etat.getJoueurs().size();
        }

    
        
        System.out.println("FIN DE JEU \n" + etat.toString());
        System.out.println("le score final est : " + etat.getFeux().score());
        this.score=etat.getFeux().score();


    }

    /**
     * Cette méthode vérifie si une chaîne de caractères peut être convertie en un entier.
     * @param str La chaîne de caractères à vérifier.
     * @return true si la chaîne peut être convertie en entier, false autrement.
     */
    public static boolean isInteger(String str) {
        if (str == null) {
            return false; // Retourne false si la chaîne est null
        }
        try {
            int num = Integer.parseInt(str); // Essaie de convertir la chaîne en entier
            return true; // Retourne true si la conversion réussit
        } catch (NumberFormatException e) {
            return false; // Retourne false si une NumberFormatException est levée
        }
    }

    /**
     * Donne des indices sur les cartes d'une certaine couleur dans la main d'un autre joueur.
     * @param autreJoueur Le joueur dont on veut connaître les cartes.
     * @param couleur La couleur des cartes pour lesquelles on veut des indices.
     * @return Une chaîne décrivant le nombre et les positions des cartes de la couleur spécifiée.
     */
    public String donnerIndiceCouleur(Joueur autreJoueur, String couleur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices = "";
        int compt = 0;
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getCouleur().equals(couleur)) {
                    carte.revelerCouleur(); // Révèle la couleur de la carte
                    compt++; // Incrémente le compteur de cartes trouvées
                    indices += " " + mainAutreJoueur.indexOf(carte); // Ajoute l'indice de la carte aux indices
                }
            }
        }
        if (compt != 0)
            return " a " + compt + " cartes de couleur " + couleur + " à l'indice " + indices;
        return null; // Retourne null si aucune carte correspondante n'est trouvée
    }

    /**
     * Donne des indices sur les cartes d'une certaine valeur dans la main d'un autre joueur.
     * @param autreJoueur Le joueur dont on veut connaître les cartes.
     * @param valeur La valeur des cartes pour lesquelles on veut des indices.
     * @return Une chaîne décrivant le nombre et les positions des cartes de la valeur spécifiée.
     */
    public String donnerIndiceValeur(Joueur autreJoueur, int valeur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices = "";
        int compt = 0;
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getValeur() == valeur) {
                    carte.revelerValeur(); // Révèle la valeur de la carte
                    compt++; // Incrémente le compteur de cartes trouvées
                    indices += " " + mainAutreJoueur.indexOf(carte); // Ajoute l'indice de la carte aux indices
                }
            }
        }
        if (compt != 0)
            return " a " + compt + " cartes de valeur " + valeur + " à l'indice " + indices;
        return null; // Retourne null si aucune carte correspondante n'est trouvée
    }

    public Etat getEtat(){
        return this.etat;
    }
    public void remplacerEtat(Etat nouvelEtat) {
        this.etat = nouvelEtat;
    }


    /**
     * Retourne le score actuel.
     * @return Le score actuel du jeu.
     */
    public int getScore() {
        return this.score; // Retourne la valeur actuelle du score
    }



}
