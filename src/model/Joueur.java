package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Représente un joueur dans le jeu Hanabi.
 * Implémente l'interface Observable pour gérer les observateurs.
 */
public class Joueur implements Observable {
    private String nom;
    private ArrayList<Carte> main;
    private boolean changed = false;
    private List<Observer> observers = new ArrayList<>();

    /**
     * Constructeur de la classe `Joueur`.
     * @param nom Le nom du joueur.
     */
    public Joueur(String nom) {
        this.nom = nom;
        this.main = new ArrayList<Carte>();
    }

    // Méthodes d'accès aux attributs de la classe Joueur
    /**
     * Retourne le nom du joueur.
     * @return Le nom du joueur.
     */

    public String getNom() {
        return nom;
    }

    /**
     * Retourne la main du joueur.
     * @return La liste des cartes dans la main du joueur.
     */
    public ArrayList<Carte> getMain() {
        return main;
    }
    
    /**
     * Crée une copie de la main du joueur en utilisant la méthode clone() pour chaque carte.
     * @return Une liste contenant des copies de chaque carte de la main.
     */
    public List<Carte> copyMain() {
         List<Carte> mainClone = new ArrayList<>();
        for (Carte carte : main) {
            mainClone.add((Carte) carte.clone());
        }
        return mainClone;
    }
    
    /**
     * Crée une copie de la main du joueur en créant de nouvelles instances de Carte pour chaque carte.
     * @return Une liste contenant des copies de chaque carte de la main.
     */
    public List<Carte> copyMainAncienne() {
        List<Carte> mainClone = new ArrayList<>();
        for (Carte carte : main) {
            mainClone.add(new Carte(carte.getCouleur(),carte.getValeur()));
        }
        return mainClone;
    }

    /**
     * Affiche les cartes dans la main du joueur avec leurs indices.
     */
    public void montrermain(){
        System.out.println("Joueur : "+nom);
        for (Carte carte : getMain()) {
               System.out.println(carte.toString() + " indice : " + getIndiceCarte(carte));
           }
    }
    
    /**
     * Ajoute une carte à la main du joueur et notifie les observateurs.
     * @param carte La carte à ajouter à la main.
     */

    public void piocherCarte(Carte carte) {
        main.add(carte);
        notifierObservateurs();
    }
    /**
     * Joue une carte de la main du joueur en fonction de l'indice spécifié.
     * La carte est retirée de la main et renvoyée.
     * @param index L'indice de la carte à jouer dans la main.
     * @return La carte jouée, ou null si l'indice est invalide.
     */
    public Carte jouerCarte(int index) {
        if (index >= 0 && index < main.size()) {
            Carte carte= main.remove(index);
            //notifierObservateurs();
            return carte;

        } else {
            return null;
        }
    }
    /**
     * Défausse une carte de la main du joueur en fonction de l'indice spécifié.
     * La carte est retirée de la main et renvoyée.
     * @param index L'indice de la carte à défausser dans la main.
     * @return La carte défaussée, ou null si l'indice est invalide.
     */
    public Carte defausserCarte(int index) {
        if (index >= 0 && index < main.size()) {
            Carte carte= main.remove(index);
            //notifierObservateurs();
            return carte;
        } else {
            return null;
        }
    }
    /**
     * Vérifie si la main du joueur contient une carte spécifique avec la couleur et la valeur données.
     * @param couleur La couleur de la carte à rechercher.
     * @param valeur La valeur de la carte à rechercher.
     * @return L'indice de la carte dans la main si elle est trouvée, sinon -1.
     */
    public int MainContains(String couleur,int valeur){
        for (Carte c : getMain())
           if( c.getCouleur().equals(couleur) && c.getValeur()==valeur)
               return getMain().indexOf(c);
        return -1;
    }
    /**
     * Vérifie si la main du joueur contient une carte de valeur 5.
     * @return true si la main contient une carte de valeur 5, sinon false.
     */
    public boolean MainContains5(){
        for (Carte c : getMain())
           if(  c.getValeur()==5)
               return true;
        return false;
    }
      /**
 * Affiche les cartes révélées dans la main du joueur avec leurs indices.
 */
   public void montrerMainRevelee() {
    System.out.println("Cartes révélées dans la main du Joueur " + this.getNom() + ":");

    for (Carte carte : main) {
        if (carte.estReveleeCouleur()) {
            if (carte.estReveleeValeur()) {
                System.out.println(carte + "a l'indice " + main.indexOf(carte) ) ;
            } else {
                System.out.println("Couleur : " + carte.getCouleur() +" a l'indice "+ main.indexOf(carte) );
            }
        } else {
            if (carte.estReveleeValeur()) {
                System.out.println("Valeur : " + carte.getValeur() +" a l'indice "+ main.indexOf(carte) );
            }
        }
    }
}
/**
 * Obtient les indices des cartes jouables dans la main du joueur pour un feu d'artifice donné.
 * @param feux Le feu d'artifice sur lequel les cartes doivent être jouables.
 * @return Une liste d'indices des cartes jouables dans la main.
 */
public List<Integer> getIndicesCartesJouables(FeuArtifice feux) {
        List<Integer> indicesCartesJouables = new ArrayList<>();

        for (int i = 0; i < main.size(); i++) {
            Carte carte = main.get(i);

            // Vérifier si la carte peut être jouée sur un feu d'artifice
            if (feux.estCarteJouable(carte)) {
                indicesCartesJouables.add(i);
            }
        }

        return indicesCartesJouables;
}
/**
 * Obtient les indices des cartes révélées dans la main du joueur.
 * @return Une liste d'indices des cartes révélées dans la main.
 */
public List<Integer> getIndicesCartesrevelees() {
        List<Integer> indicesCartesrevelees = new ArrayList<>();

        for (int i = 0; i < main.size(); i++) {
            Carte carte = main.get(i);
            if (carte.estReveleeCouleur() && carte.estReveleeValeur()) 
                   indicesCartesrevelees.add(i);   
            }
        return indicesCartesrevelees;
}
/**
 * Obtient les indices des cartes révélées par valeur dans la main du joueur.
 * @return Une liste d'indices des cartes révélées par valeur dans la main.
 */
public List<Integer> getIndicesCartesreveleesParValeur() {
        List<Integer> indicesCartesrevelees = new ArrayList<>();

        for (int i = 0; i < main.size(); i++) {
            Carte carte = main.get(i);
            if (carte.estReveleeValeur()) 
                   indicesCartesrevelees.add(i);   
            }
        return indicesCartesrevelees;
}
/**
 * Obtient les indices des cartes révélées par couleur dans la main du joueur.
 * @return Une liste d'indices des cartes révélées par couleur dans la main.
 */
public List<Integer> getIndicesCartesreveleesParCouleur() {
        List<Integer> indicesCartesrevelees = new ArrayList<>();

        for (int i = 0; i < main.size(); i++) {
            Carte carte = main.get(i);
            if (carte.estReveleeCouleur()) 
                   indicesCartesrevelees.add(i);   
            }
        return indicesCartesrevelees;
}
/**
 * Obtient les indices des cartes non révélées dans la main du joueur.
 * @return Une liste d'indices des cartes non révélées dans la main.
 */
public List<Integer> getIndicesCartesNonrevelees() {
        List<Integer> indicesCartesNonrevelees = new ArrayList<>();

        for (int i = 0; i < main.size(); i++) {
            Carte carte = main.get(i);
            if (!carte.estReveleeCouleur() && !carte.estReveleeValeur()) 
                   indicesCartesNonrevelees.add(i);   
            }
        return indicesCartesNonrevelees;
}

public boolean aCarteJouable(FeuArtifice feux) {
    for (Carte carte : main) {
        if (feux.estCarteJouable(carte)) {
            return true;  // Si au moins une carte est jouable, retourne true
        }
    }
    return false;  // Si aucune carte n'est jouable, retourne false
}

public boolean aCarterevelee() {
  for (Carte carte : getMain()) {
        if (carte.estReveleeCouleur() && carte.estReveleeValeur()) 
            // La carte est révélée en couleur ou en valeur, elle est utile, ne pas défausser
            return true;
        }
    return false; // Si aucune carte n'est jouable, retourne false
}

public int getIndiceCarte(Carte carteCliquee){
    for (int i = 0; i < main.size(); i++) {
            Carte carte = main.get(i);
            if (carte.getCouleur().equals(carteCliquee.getCouleur()) && carte.getValeur()==carteCliquee.getValeur()) 
                   return i;   
            }
        return -1; // Si aucune carte n'est jouable, retourne false
}

// Méthode pour notifier les observateurs après des modifications
    private void notifierObservateurs() {
        setChanged();
        notifyObservers();
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "nom='" + nom + '\'' +
                ", main=" + main +
                '}';
    }
     @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        if (!changed) return;

        for (Observer observer : observers) {
            observer.update(this, null);
        }

        clearChanged();
    }

    

    @Override
    public void setChanged() {
        changed = true;
    }

    private void clearChanged() {
        changed = false;
    }
     
    
    public Joueur copy() {
    Joueur copieJoueur = new Joueur(nom);
    for (Carte carte : main) {
        Carte copieCarte = new Carte(carte.getCouleur(), carte.getValeur()); // Crée une nouvelle instance de Carte avec les mêmes valeurs
        copieJoueur.getMain().add(copieCarte);  // Ajoute la carte copiée à la main du joueur copié
    }
    return copieJoueur;
}

    
}
