package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 * Représente un paquet de cartes dans le jeu Hanabi.
 * Implémente l'interface Observable pour gérer les observateurs.
 */
public class Paquet implements Observable {
    private ArrayList<Carte> cartes;
    private boolean changed = false;
    private List<Observer> observers = new ArrayList<>();
    /**
     * Constructeur par défaut de la classe `Paquet`.
     * Initialise le paquet de cartes et le mélange.
     */
    public Paquet() {
        this.cartes = new ArrayList<Carte>();
        initialiserPaquet();
        melanger();
    }
    /**
     * Initialise le paquet de cartes selon les règles du jeu Hanabi.
     */
    private void initialiserPaquet() {
        // Ajouter des cartes au deck
        // une initialisation de paquet selon le jeu hanabi
        String[] couleurs = { "Rouge", "Bleu", "Vert", "Jaune", "Blanc" };
        int[] valeurs = { 1, 1, 1, 2, 2, 3, 3, 4, 4, 5 };
        for (String couleur : couleurs) {
            for (int valeur : valeurs) {
                cartes.add(new Carte(couleur, valeur));
            }
        }
    }
    /**
     * Mélange les cartes dans le paquet.
     * Notifie les observateurs après le mélange.
     */
    private void melanger() {
        Collections.shuffle(cartes);
        setChanged();
        notifyObservers(); // Notifier les observateurs après le mélange
    }
    /**
     * Pioche une carte dans le paquet.
     * @return La carte piochée, ou null si le paquet est vide.
     */
    public Carte piocher() {
        if (cartes.isEmpty()) {
            return null;
        }
        Carte carte = cartes.remove(0);
        setChanged();
        notifyObservers(); // Notifier les observateurs après le piochage
        return carte;
    }
    /**
     * Obtient la taille du paquet.
     * @return Le nombre de cartes dans le paquet.
     */
    public int taille() {
        return cartes.size();
    }
    /**
     * Obtient la liste des cartes dans le paquet.
     * @return La liste des cartes.
     */
    public ArrayList<Carte> getCartes() {
        return this.cartes;
    }

    // Implémentation des méthodes de l'interface Observable
     @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        if (!changed) return;

        for (Observer observer : observers) {
            observer.update(this, null);
        }

        clearChanged();
    }

    public void setCartesVide(){
        this.cartes.clear();
    }

    @Override
    public void setChanged() {
        changed = true;
    }

    private void clearChanged() {
        changed = false;
    }
    
    /**
     * Crée une copie du paquet de cartes.
     * @return Une copie du paquet de cartes.
     */
    public Paquet copy() {
        Paquet copiePaquet = new Paquet();
        copiePaquet.setCartesVide();
        for (int i = 0 ; i< cartes.size(); i++) {
            Carte carte = cartes.get(i);
            copiePaquet.getCartes().add(i, new Carte(carte.getCouleur(), carte.getValeur())); 
        }
        return copiePaquet;
    }

}
