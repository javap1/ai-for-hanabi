package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

// Classe représentant une stratégie scoreMax pour le jeu.
public class ScoreMax implements Strategie {

    // Constantes définissant les différentes actions possibles
    private static final int DONNER_INDICE_ACTION = 1;
    private static final int JOUER_CARTE_ACTION = 2;
    private static final int DEFAUSSER_CARTE_ACTION = 3;
   
    // Constructeur initialisant la stratégie
    public ScoreMax() {
        }

    /**
     * Méthode principale décidant de l'action à effectuer en fonction de l'état du jeu et du joueur courant.
     *
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return L'action à effectuer.
     */    
    public Action actionAfaire(Joueur joueurCourant, Etat etat) {
            // Choix de l'action basé sur la situation actuelle
            int choix = choisirAction(joueurCourant, etat);

            // Exécution de l'action choisie
            switch (choix) {
                case DONNER_INDICE_ACTION:
                    return donnerIndice(joueurCourant, etat);
                case JOUER_CARTE_ACTION:
                    return jouerCarte(joueurCourant, etat);
                case DEFAUSSER_CARTE_ACTION:
                    return defausserCarte(joueurCourant, etat);
                default:
                    throw new IllegalArgumentException("Choix invalide: " + choix);
            }
        }
    /**
     * Méthode pour choisir une des trois actions possibles.
     *
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return L'action choisie (1: donner un indice, 2: jouer une carte, 3: défausser une carte).
     */
    private int choisirAction(Joueur joueurCourant, Etat etat) {
            
            
            // Si le joueur a une carte inutile et qu'il n'y a pas de jetons bleus, il doit défausser
            if (aCarteInutile(joueurCourant, etat.getFeux()) != -1 && etat.getJetonsBleu() == 0) {
                return DEFAUSSER_CARTE_ACTION;
            }

            // Si le joueur a une carte jouable, il doit la jouer
            if (aCarteJouable(joueurCourant, etat.getFeux()) != -1) {
                return JOUER_CARTE_ACTION;
            } 

            // Si une carte jouable ou dangereuse existe et qu'il y a des jetons bleus, donner un indice
            else if ((CarteJouable(joueurCourant, etat, etat.getFeux()) || existeCarteDangereuse(joueurCourant, etat)) && etat.getJetonsBleu() > 0) {
                return DONNER_INDICE_ACTION;
            } 

            // Sinon, défausser une carte
            else {
                return DEFAUSSER_CARTE_ACTION;
            }
        }

        
    /**
     * Donne un indice à un joueur en fonction de l'évaluation de ses cartes.
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return Action à effectuer,Essentiellement donner un indice, au cas d'indice non trouvé défausser une carte.
     */

    public Action donnerIndice(Joueur joueurCourant, Etat etat) {
        String indice = null;

        // Parcourt chaque joueur, sauf le joueur courant, pour potentiellement donner un indice
        for (Joueur autreJoueur : etat.getJoueurs()) {
            if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
                // Parcourt chaque carte dans la main de l'autre joueur
                for (Carte carte : autreJoueur.getMain()) {
                    // Si la carte est jugée jouable mais ne peut pas être défaussée
                    if (etat.getFeux().estCarteJouable(carte) && !peutDefausserCarte(carte, etat.getCartesDefausses())) {
                        // Si la valeur de la carte n'est pas révélée, on tente de donner un indice sur sa valeur
                        if (!carte.estReveleeValeur()) {
                            indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                            // Vérifie si l'indice n'a pas déjà été donné
                            if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                                return new Action(autreJoueur.getNom(), String.valueOf(carte.getValeur()));
                            }
                        }
                        // Si la couleur de la carte n'est pas révélée, on tente de donner un indice sur sa couleur
                        if (!carte.estReveleeCouleur()) {
                            indice = donnerIndiceCouleur(autreJoueur, carte.getCouleur());
                            // Vérifie si l'indice n'a pas déjà été donné
                            if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                                return new Action(autreJoueur.getNom(), carte.getCouleur());
                            }
                        }
                    }
                }
            }
        } // Parcourt chaque joueur, sauf le joueur courant, pour potentiellement donner un indice
        for (Joueur autreJoueur : etat.getJoueurs()) {
            if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
                // Parcourt chaque carte dans la main de l'autre joueur
                for (Carte carte : autreJoueur.getMain()) {
                    // Si la carte est jugée jouable mais on peut la défaussée
                    if (etat.getFeux().estCarteJouable(carte) && peutDefausserCarte(carte, etat.getCartesDefausses())) {
                        // Si la valeur de la carte n'est pas révélée, on tente de donner un indice sur sa valeur
                        if (!carte.estReveleeValeur()) {
                            indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                            // Vérifie si l'indice n'a pas déjà été donné
                            if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                                return new Action(autreJoueur.getNom(), String.valueOf(carte.getValeur()));
                            }
                        }
                        // Si la couleur de la carte n'est pas révélée, on tente de donner un indice sur sa couleur
                        if (!carte.estReveleeCouleur()) {
                            indice = donnerIndiceCouleur(autreJoueur, carte.getCouleur());
                            // Vérifie si l'indice n'a pas déjà été donné
                            if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                                return new Action(autreJoueur.getNom(), carte.getCouleur());
                            }
                        }
                    }
                }
            }
        }
            // Parcourt chaque joueur, sauf le joueur courant, pour potentiellement donner un indice
        for (Joueur autreJoueur : etat.getJoueurs()) {
            if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
                // Parcourt chaque carte dans la main de l'autre joueur
                for (Carte carte : autreJoueur.getMain()) {
                
                    if (!peutDefausserCarte(carte, etat.getCartesDefausses())) {
                        // Si la valeur de la carte n'est pas révélée, on tente de donner un indice sur sa valeur
                        if (!carte.estReveleeValeur()) {
                            indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                            // Vérifie si l'indice n'a pas déjà été donné
                            if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                                return new Action(autreJoueur.getNom(), String.valueOf(carte.getValeur()));
                            }
                        }
                        // Si la couleur de la carte n'est pas révélée, on tente de donner un indice sur sa couleur
                        if (!carte.estReveleeCouleur()) {
                            indice = donnerIndiceCouleur(autreJoueur, carte.getCouleur());
                            // Vérifie si l'indice n'a pas déjà été donné
                            if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                                return new Action(autreJoueur.getNom(), carte.getCouleur());
                            }
                        }
                    }
                }
            }
        }
        // Aucun indice trouvé ou déjà donné, on défausse une carte
        System.out.println("Indice non trouvé ou déjà donné. On défausse une carte.");
        return defausserCarte(joueurCourant, etat);
    }



        
    /**
     * Méthode pour jouer une carte dans la main du joueur courant.
     *
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return L'action de jouer une carte.
     */
    public Action jouerCarte(Joueur joueurCourant, Etat etat){
  
        // Choisir une carte à jouer en fonction de la logique implémentée dans choisirCarteAJouer
        int indiceCarteAJouer = choisirCarteAJouer(joueurCourant, etat);

        // Si aucune carte spécifique n'est choisie, sélectionner une carte aléatoirement
        if(indiceCarteAJouer == -1){
            indiceCarteAJouer = new Random().nextInt(joueurCourant.getMain().size()); // Choix aléatoire parmi les cartes disponibles
        }

        // Retourner l'action de jouer la carte choisie
        return new Action(indiceCarteAJouer);
    }

    /**
     * Méthode pour défausser une carte dans la main du joueur courant.
     *
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return L'action de défausser une carte.
     */
    public Action defausserCarte(Joueur joueurCourant, Etat etat){

        // Choisir une carte à défausser en fonction de la logique implémentée dans choisirCarteADefausser
        int indiceCarteAdefausser = choisirCarteADefausser(joueurCourant, etat.getFeux(), etat.getCartesDefausses());

        // Si aucune carte spécifique n'est choisie, sélectionner une carte par défaut ou en fonction d'une autre logique
        if(indiceCarteAdefausser == -1){
            indiceCarteAdefausser = 1; // Choix par défaut ou selon une autre logique
            for(int indice : joueurCourant.getIndicesCartesreveleesParValeur()) {
                if(joueurCourant.getMain().get(indice).getValeur() != 5) {
                    indiceCarteAdefausser = indice; // Mise à jour de la carte à défausser
                }
            }
        }

        // Retourner l'action de défausser la carte choisie
        return new Action(3, indiceCarteAdefausser);
    }

    /**
     * Méthode pour vérifier si une carte est jouable.
     *
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @param feux Les feux représentant les cartes jouées.
     * @return true si une carte jouable est trouvée, false sinon.
     */
    public boolean CarteJouable(Joueur autrejoueur, Etat etat, FeuArtifice feux) {
        // Parcourir tous les joueurs sauf le joueur courant
        for (Joueur joueur : etat.getJoueurs()) {
            if (!joueur.getNom().equals(autrejoueur.getNom())) {
                // Vérifier si une carte dans la main du joueur est jouable
                for (Carte carte : joueur.getMain()) {
                    if (feux.estCarteJouable(carte)) {
                        return true; // Carte jouable trouvée
                    }
                }
            }
        }
        return false; // Aucune carte jouable trouvée
    }

    
    /**
     * Méthode pour vérifier l'existence de cartes dangereuses (non défaussables) chez les autres joueurs
     *
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return true si une carte dangereuse est trouvée, false sinon.
     */
    public boolean existeCarteDangereuse(Joueur joueurCourant, Etat etat) {
        // Parcourir tous les joueurs sauf le joueur courant
        for (Joueur joueur : etat.getJoueurs()) {
            if (!joueur.getNom().equals(joueurCourant.getNom())) {
                // Vérifier si une carte dans la main du joueur est dangereuse
                for (Carte carte : joueur.getMain()) {
                    if (!peutDefausserCarte(carte, etat.getCartesDefausses())) {
                        return true; // Carte dangereuse trouvée
                    }
                }
            }
        }
        return false; // Aucune carte dangereuse trouvée
    }



    /**
     * Détermine si une carte peut être défaussée en fonction des cartes déjà présentes dans la défausse.
     *
     * @param carte La carte à évaluer pour la défausse.
     * @param defausse La liste des cartes qui ont été déjà défaussées.
     * @return true si la carte peut être défaussée, false sinon.
     */
    public static boolean peutDefausserCarte(Carte carte, List<Carte> defausse) {
        int count = 0; // Compteur pour le nombre de copies identiques de la carte dans la défausse

        // Détermine le nombre maximal de doublons autorisés pour cette carte en fonction de sa valeur
        int maxDuplicates = (carte.getValeur() == 1) ? 3 : (carte.getValeur() >= 2 && carte.getValeur() <= 4) ? 2 : 1;

        // Parcourt la pile de défausse pour compter le nombre de cartes identiques (même valeur et couleur)
        for (Carte carteDefaussee : defausse) {
            if (carte.getValeur() == carteDefaussee.getValeur() && carte.getCouleur().equals(carteDefaussee.getCouleur())) {
                count++;
            }
        }

        // La carte peut être défaussée si le nombre de doublons dans la défausse n'a pas atteint le maximum autorisé
        return count < maxDuplicates;
    }

    /**
     * Vérifie si une carte est inutile dans la main du joueur.
     * Retourne l'indice de la première carte inutile trouvée, sinon retourne -1.
     *
     * @param joueur         Le joueur actuel.
     * @param feux           L'état actuel des feux.
     * @param cartesDefausses Les cartes déjà défaussées.
     * @return L'indice de la carte inutile, ou -1 si aucune carte n'est inutile.
     */
    public Integer aCarteInutile(Joueur joueur, FeuArtifice feux) {
        for (Carte carte : joueur.getMain()) {
            if (feux.estCarteInutile(carte)) {
                // Retourne l'indice de la première carte jouable trouvée
                return joueur.getIndiceCarte(carte);
            }
        }

        

        // Si aucune carte inutile n'est trouvée, retourne -1.
        return -1;
    }

    
    /**
     * Choisit une carte à défausser en évaluant les cartes de la main du joueur.
     * Retourne l'indice de la première carte inutile trouvée, sinon retourne un indice aléatoire de carte non révélée.
     *
     * @param joueur         Le joueur actuel.
     * @param feux           L'état actuel des feux.
     * @param cartesDefausses Les cartes déjà défaussées.
     * @return L'indice de la carte à défausser.
     */
    public int choisirCarteADefausser(Joueur joueur, FeuArtifice feux, List<Carte> cartesDefausses) {
        // Détermine si une carte inutile est présente dans la main du joueur
        Integer indiceCarteInutile = aCarteInutile(joueur, feux);

        // Si une carte inutile est trouvée, retourne son indice
        if (indiceCarteInutile != -1) {
            return indiceCarteInutile;
        }

        // Si aucune carte inutile n'est trouvée, choisit un indice aléatoire parmi les cartes non révélées
        List<Integer> indicesCartesNonRevelees = joueur.getIndicesCartesNonrevelees();
        if (!indicesCartesNonRevelees.isEmpty()) {
            return indicesCartesNonRevelees.get(new Random().nextInt(indicesCartesNonRevelees.size()));
        }

        // Si aucune carte non révélée n'est disponible, retourne -1
        return -1;
    }

    /**
     * Détermine si une carte est jouable dans la main du joueur en fonction de l'état actuel des feux.
     *
     * @param joueur Le joueur actuel.
     * @param feux   L'état actuel des feux.
     * @return L'indice de la première carte jouable trouvée, sinon retourne -1.
     */
    public Integer aCarteJouable(Joueur joueur, FeuArtifice feux) {
        

        // Parcourt les cartes révélées et détermine si elles sont jouables
        for (Carte carte : joueur.getMain()) {
            if (feux.estCarteJouable(carte)) {
                // Retourne l'indice de la première carte jouable trouvée
                return joueur.getIndiceCarte(carte);
            }
        }

        // Si aucune carte jouable n'est trouvée, retourne -1
        return -1;
    }

    /**
     * Choisit une carte à jouer en se basant sur les cartes jouables dans la main du joueur.
     *
     * @param joueur Le joueur actuel.
     * @param etat   L'état actuel du jeu.
     * @return L'indice de la première carte jouable trouvée.
     */
    public int choisirCarteAJouer(Joueur joueur, Etat etat) {
        // Utilise la méthode aCarteJouable pour trouver une carte jouable
        return aCarteJouable(joueur, etat.getFeux());
    }


    /**
     * Donne un indice sur la couleur des cartes d'un autre joueur.
     *
     * @param autreJoueur Le joueur dont les cartes sont évaluées.
     * @param couleur     La couleur des cartes recherchée.
     * @return Une chaîne de caractères indiquant le nombre de cartes de la couleur donnée et leurs indices dans la main du joueur, ou null si aucune carte n'est trouvée.
     */
    public String donnerIndiceCouleur(Joueur autreJoueur, String couleur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices = "";
        int compt = 0;
        
        // Parcourt la main de l'autre joueur pour trouver les cartes de la couleur spécifiée
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getCouleur().equals(couleur)) {
                    compt++;
                    indices += " " + mainAutreJoueur.indexOf(carte);
                }
            }
        }

        // Si des cartes de la couleur spécifiée sont trouvées, retourne les informations, sinon retourne null
        if (compt != 0)
            return " a " + compt + " cartes de couleur " + couleur + " à l'indice " + indices;
        return null;
    }

    /**
     * Donne un indice sur la valeur des cartes d'un autre joueur.
     *
     * @param autreJoueur Le joueur dont les cartes sont évaluées.
     * @param valeur      La valeur des cartes recherchée.
     * @return Une chaîne de caractères indiquant le nombre de cartes de la valeur donnée et leurs indices dans la main du joueur, ou null si aucune carte n'est trouvée.
     */
    public String donnerIndiceValeur(Joueur autreJoueur, int valeur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices = "";
        int compt = 0;
        
        // Parcourt la main de l'autre joueur pour trouver les cartes de la valeur spécifiée
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getValeur() == valeur) {
                    compt++;
                    indices += " " + mainAutreJoueur.indexOf(carte);
                }
            }
        }
        
        // Si des cartes de la valeur spécifiée sont trouvées, retourne les informations, sinon retourne null
        if (compt != 0)
            return " a " + compt + " cartes de valeur " + valeur + " à l'indice " + indices;
        return null;
    }

    /**
     * Retourne une description de la stratégie.
     *
     * @return Une chaîne de caractères indiquant la stratégie utilisée.
     */
    public String toString() {
        return " C'est Strategie Statistique";
    }


}