package model;


public interface Strategie {
    Action actionAfaire(Joueur joueurCourant, Etat etat);
}
