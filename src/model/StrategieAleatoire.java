package model;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class StrategieAleatoire implements Strategie {
    // Constantes pour représenter les différentes actions
    private static final int DONNER_INDICE_ACTION = 1;
    private static final int JOUER_CARTE_ACTION = 2;
    private static final int DEFAUSSER_CARTE_ACTION = 3;

    public StrategieAleatoire(){
        
    }
    /**
     * Sélectionne une action à effectuer pour le joueur courant et crée l'action correspondante.
     * @param joueurCourant Le joueur pour lequel une action doit être sélectionnée.
     * @param etat L'état actuel du jeu.
     * @return Une action à effectuer pour le joueur courant.
     */
    @Override
    public Action actionAfaire(Joueur joueurCourant, Etat etat) {
        Random random = new Random();
        int choix = random.nextInt(3) + 1; // Génère un nombre aléatoire entre 1 et 3 inclus
        switch (choix) {
            case DONNER_INDICE_ACTION:
                return donnerIndiceAleatoire(joueurCourant, etat);
            case JOUER_CARTE_ACTION:
                return jouerCarteAleatoire(joueurCourant, etat);
            case DEFAUSSER_CARTE_ACTION:
                return defausserCarteAleatoire(joueurCourant, etat);
            default:
                throw new IllegalArgumentException("Choix invalide: " + choix);
        }
    } 

    /**
     * Sélectionne un indice aléatoire à donner à un joueur et crée l'action correspondante.
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return Une action représentant l'indice aléatoire donné.
     */

    public Action donnerIndiceAleatoire(Joueur joueurCourant, Etat etat) {
    if(etat.getJetonsBleu()==0)
     {
        System.out.println("Faute de jeu :TU Peux pas donner indice car Jetons bleus =0 \n TU vas devoir defausser une carte");
        return defausserCarteAleatoire(joueurCourant, etat);

     }
    // Initialisation d'un générateur de nombres aléatoires
    Random random = new Random();

    // Sélection aléatoire entre couleur (0) ou valeur (1) pour l'indice
    int couleurOuValeur = random.nextInt(2); // 0 ou 1

    // Récupération de la liste des joueurs
    List<Joueur> joueurs = etat.getJoueurs();

    // Sélection aléatoire d'un joueur parmi la liste des joueurs disponibles
    Joueur joueurAleatoire;
    do {
        joueurAleatoire = joueurs.get(random.nextInt(joueurs.size()));
    } while (!joueurAleatoire.getNom().equals(joueurCourant.getNom())); // Sélectionne un joueur différent du joueur courant

    // Sélection aléatoire d'une carte dans la main du joueur sélectionné
    int indiceCartePourIndice = random.nextInt(4); // Indice aléatoire entre 0 et 3 pour sélectionner une carte
    Carte cartePourIndice = joueurAleatoire.getMain().get(indiceCartePourIndice);

    if (couleurOuValeur == 0) {
        // Action de donner un indice de couleur aléatoire
        return new Action(joueurAleatoire.getNom(), cartePourIndice.getCouleur());
    } else {
        // Action de donner un indice de valeur aléatoire
        return new Action(joueurAleatoire.getNom(), String.valueOf(cartePourIndice.getValeur()));
    }
}

    /**
     * Sélectionne une carte à jouer aléatoirement pour le joueur courant et crée l'action correspondante.
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return Une action représentant la carte jouée aléatoirement.
     */
    private Action jouerCarteAleatoire(Joueur joueurCourant, Etat etat) {
        // Action de jouer une carte aléatoire
        Random random = new Random();
        int indiceCarteAJouer = random.nextInt(4); // Génère un nombre aléatoire entre 0 et 3 inclus
        return new Action(indiceCarteAJouer);
    }
    /**
     * Sélectionne une carte à défausser aléatoirement pour le joueur courant et crée l'action correspondante.
     * @param joueurCourant Le joueur courant.
     * @param etat L'état actuel du jeu.
     * @return Une action représentant la carte défaussée aléatoirement.
     */
    private Action defausserCarteAleatoire(Joueur joueurCourant, Etat etat) {
        // Action de défausser une carte aléatoire
        Random random = new Random();
        int indiceCarteADefausser = random.nextInt(4); // Génère un nombre aléatoire entre 0 et 3 inclus
        return new Action(3, indiceCarteADefausser);
    }

    public String toString(){
     return " C'est Strategie Aleatoire";
    }
}
