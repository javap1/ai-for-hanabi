package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StrategieObservatrice implements Strategie {
    private static final int DONNER_INDICE_ACTION = 1;
    private static final int JOUER_CARTE_ACTION = 2;
    private static final int DEFAUSSER_CARTE_ACTION = 3;

   
    public StrategieObservatrice() {
        
    }

   /**
     * Sélectionne une action à effectuer pour le joueur en fonction de l'état actuel du jeu.
     * @param joueurCourant Le joueur actuel.
     * @param etat L'état actuel du jeu.
     * @return L'action à effectuer.
     */
   public Action actionAfaire(Joueur joueurCourant, Etat etat) {
        // Choix de l'action en fonction de l'état actuel du jeu
        int choix = choisirAction(joueurCourant, etat);

        switch (choix) {
            case DONNER_INDICE_ACTION:
                return donnerIndice(joueurCourant, etat);
            case JOUER_CARTE_ACTION:
                return jouerCarte(joueurCourant, etat);
            case DEFAUSSER_CARTE_ACTION:
                return defausserCarte(joueurCourant, etat);
            default:
                throw new IllegalArgumentException("Choix invalide: " + choix);
        }
    }

/**
 * Choix de l'action à effectuer en fonction de l'état actuel du jeu.
 * @param joueurCourant Le joueur actuel.
 * @param etat L'état actuel du jeu.
 * @return Le numéro de l'action à effectuer.
 */
private int choisirAction(Joueur joueurCourant,Etat etat ) {
    // Vérifie si une carte est inutile et s'il reste des jetons bleus
    if (aCarteInutile(joueurCourant, etat.getFeux(),etat.getCartesDefausses())!=-1 && etat.getJetonsBleu() < 8)
        return DEFAUSSER_CARTE_ACTION;
    // Vérifie si une carte est jouable immédiatement
    if (aCarteJouableImmediat(joueurCourant, etat.getFeux(),etat.getCartesDefausses())!=-1) {
        return JOUER_CARTE_ACTION;
    // Vérifie si une carte est jouable immédiatement, dangereuse ou jouable plus tard et s'il reste des jetons bleus
    } else if ((existeCarteJouableImmediat(joueurCourant, etat) || existeCarteDangereuse(joueurCourant, etat) || existeCarteJouableTard(joueurCourant, etat) )&& etat.getJetonsBleu() > 0) {
        return DONNER_INDICE_ACTION;
    // Si plus d'un jeton rouge est restant, joue une carte
    } else if(etat.getJetonsRouge()>1)
        return JOUER_CARTE_ACTION;
     // Par défaut, défausse une carte
    else {
        return DEFAUSSER_CARTE_ACTION;
    }
}


/**
 * Donne un indice à un joueur en fonction de l'évaluation de ses cartes.
 * @param joueurCourant Le joueur courant.
 * @param etat L'état actuel du jeu.
 * @return Action à effectuer,Essentiellement donner un indice, au cas d'indice non trouvé défausser une carte.
 */
public Action donnerIndice(Joueur joueurCourant, Etat etat) {
    String indice=null;


    for (Joueur autreJoueur : etat.getJoueurs()) {
        if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
            autreJoueur.montrermain();  // Montre la main du joueur
            for (Carte carte : autreJoueur.getMain()) {
                // Évalue la carte
                EvaluationCarte evaluation = evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses());

                // Vérifie si la carte est évaluée comme JOUABLE_PLUS_TARD
                if (evaluation == EvaluationCarte.JOUABLE_IMMEDIATEMENT  ) {
                    if(!carte.estReveleeValeur()){
                        indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                        // Vérifie si l'indice est valide et n'a pas été donné auparavant
                        if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                            return new Action(autreJoueur.getNom(), String.valueOf(carte.getValeur()));
                        }
                    }
                    if(!carte.estReveleeCouleur()){
                            // Donne un indice de couleur si l'indice de valeur n'est pas possible
                        indice = donnerIndiceCouleur(autreJoueur, carte.getCouleur());
                    }

                        // Vérifie si l'indice de couleur est valide et n'a pas été donné auparavant
                    if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                            return new Action(autreJoueur.getNom(), carte.getCouleur());
                        }
                }
            }
        }
    }

    for (Joueur autreJoueur : etat.getJoueurs()) {
        if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
            autreJoueur.montrermain();  // Montre la main du joueur
            for (Carte carte : autreJoueur.getMain()) {
                // Évalue la carte

                EvaluationCarte evaluation = evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses());
                // Vérifie si la carte est évaluée comme JOUABLE_PLUS_TARD
                if (evaluation == EvaluationCarte.DANGEREUSE) {
                    if(!carte.estReveleeValeur()){
                        // Donne un indice de valeur pour une carte jouable plus tard
                        indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                        // Vérifie si l'indice est valide et n'a pas été donné auparavant
                        if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                            return new Action(autreJoueur.getNom(),String.valueOf(carte.getValeur()));
                        }
                    }
                }
            }
        }
    }

 

    // Deuxième passage pour les cartes évaluées comme JOUABLE_PLUS_TARD
    for (Joueur autreJoueur : etat.getJoueurs()) {
        if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
            for (Carte carte : autreJoueur.getMain()) {
                // Évalue la carte
                EvaluationCarte evaluation = evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses());

                // Vérifie si la carte est évaluée comme JOUABLE_PLUS_TARD
                if (evaluation == EvaluationCarte.JOUABLE_PLUS_TARD) {
                    if(!carte.estReveleeValeur()){
                        // Donne un indice de valeur pour une carte jouable plus tard
                        indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                        // Vérifie si l'indice est valide et n'a pas été donné auparavant
                        if (indice != null && !etat.containsIndice(autreJoueur, indice)) {
                            return new Action(autreJoueur.getNom(), String.valueOf(carte.getValeur()));
                        }
                    }
                }
            }
        }
    }

    // Aucun indice trouvé ou déjà donné, on défausse une carte
    System.out.println("Indice non trouvé ou déjà donné. On défausse une carte.");
    return defausserCarte(joueurCourant, etat);
}



    
/**
 * Sélectionne une carte à jouer pour le joueur courant et crée une action correspondante.
 * @param joueurCourant Le joueur qui doit jouer une carte.
 * @param etat L'état actuel du jeu.
 * @return Une action représentant la carte jouée.
 */
public Action jouerCarte(Joueur joueurCourant, Etat etat) {
    // Affiche la main actuelle du joueur
    System.out.println("Observation de la main du joueur courant " + joueurCourant.getNom() + " : ");
    joueurCourant.montrermain();
    joueurCourant.montrerMainRevelee();

    // Choisis une carte à jouer en se basant sur l'état actuel du jeu
    int indiceCarteAJouer = choisirCarteAJouer(joueurCourant, etat);

    // Si aucune carte n'est jugée jouable, en choisir une au hasard
    if (indiceCarteAJouer == -1) {
        indiceCarteAJouer = new Random().nextInt(joueurCourant.getMain().size()); // Remplacer 4 par la taille de la main si différente
    }

    // Retourne une action pour jouer la carte sélectionnée
    return new Action(indiceCarteAJouer); 
}

/**
 * Sélectionne une carte à défausser pour le joueur courant et crée une action correspondante.
 * @param joueurCourant Le joueur qui doit défausser une carte.
 * @param etat L'état actuel du jeu.
 * @return Une action représentant la carte défaussée.
 */
public Action defausserCarte(Joueur joueurCourant, Etat etat) {
    // Affiche la main actuelle du joueur
    System.out.println("Observation de la main du joueur courant " + joueurCourant.getNom() + " : ");
    joueurCourant.montrermain();
    joueurCourant.montrerMainRevelee();

    // Choisis une carte à défausser en se basant sur l'état actuel du jeu
    int indiceCarteAdefausser = choisirCarteADefausser(joueurCourant, etat.getFeux(), etat.getCartesDefausses());

    // Si aucune carte n'est jugée défaussable, en choisir une au hasard
    if (indiceCarteAdefausser == -1) {
        indiceCarteAdefausser = new Random().nextInt(joueurCourant.getMain().size()); // Remplacer 4 par la taille de la main si différente
    }

    // Retourne une action pour défausser la carte sélectionnée
    return new Action(3, indiceCarteAdefausser); // Le chiffre 3 pourrait représenter un code d'action spécifique
}

    

    
/**
 * Vérifie si un joueur autre que le joueur courant a une carte jouable immédiatement dans sa main.
 * @param joueurCourant Le joueur actuellement en train de jouer.
 * @param etat L'état actuel du jeu, incluant les joueurs, les feux d'artifice et les cartes défaussées.
 * @return true si un autre joueur a une carte jouable immédiatement, false sinon.
 */
public boolean existeCarteJouableImmediat(Joueur joueurCourant, Etat etat) {
    // Vérifier pour chaque joueur (sauf le joueur courant) si une carte jouable immédiatement existe
    return etat.getJoueurs().stream()
            .filter(joueur -> !joueur.getNom().equals(joueurCourant.getNom()))
            .flatMap(joueur -> joueur.getMain().stream())
            .anyMatch(carte -> evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses()) == EvaluationCarte.JOUABLE_IMMEDIATEMENT);
}

/**
 * Vérifie si un joueur autre que le joueur courant a une carte jouable plus tard dans sa main.
 * @param joueurCourant Le joueur actuellement en train de jouer.
 * @param etat L'état actuel du jeu, incluant les joueurs, les feux d'artifice et les cartes défaussées.
 * @return true si un autre joueur a une carte jouable plus tard, false sinon.
 */
public boolean existeCarteJouableTard(Joueur joueurCourant, Etat etat) {
    // Vérifier pour chaque joueur (sauf le joueur courant) si une carte jouable plus tard existe
    return etat.getJoueurs().stream()
            .filter(joueur -> !joueur.getNom().equals(joueurCourant.getNom()))
            .flatMap(joueur -> joueur.getMain().stream())
            .anyMatch(carte -> evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses()) == EvaluationCarte.JOUABLE_PLUS_TARD);
}

/**
 * Vérifie si un joueur autre que le joueur courant a une carte dangereuse dans sa main.
 * @param joueurCourant Le joueur actuellement en train de jouer.
 * @param etat L'état actuel du jeu, incluant les joueurs, les feux d'artifice et les cartes défaussées.
 * @return true si un autre joueur a une carte dangereuse, false sinon.
 */
public boolean existeCarteDangereuse(Joueur joueurCourant, Etat etat) {
    // Vérifier pour chaque joueur (sauf le joueur courant) si une carte dangereuse existe
    return etat.getJoueurs().stream()
            .filter(joueur -> !joueur.getNom().equals(joueurCourant.getNom()))
            .flatMap(joueur -> joueur.getMain().stream())
            .anyMatch(carte -> evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses()) == EvaluationCarte.DANGEREUSE);
}

/**
 * Enumération pour les différents types d'évaluation d'une carte.
 */
public enum EvaluationCarte {
    JOUABLE_IMMEDIATEMENT, // La carte peut être jouée immédiatement
    INUTILE,              // La carte est inutile et ne peut pas contribuer au jeu
    JOUABLE_PLUS_TARD,    // La carte peut être jouée mais à un moment ultérieur
    DANGEREUSE,           // La carte est dangereuse et son jeu peut être risqué
    JESAISPAS             // Incertitude sur la manière d'utiliser la carte
}

/**
 * Évalue la carte en fonction de sa jouabilité et de sa pertinence dans le jeu actuel.
 * @param carte La carte à évaluer.
 * @param feux L'état actuel des feux d'artifice.
 * @param carteDefausses La liste des cartes déjà défaussées.
 * @return L'évaluation de la carte en fonction des règles du jeu.
 */
public EvaluationCarte evaluerCarte(Carte carte, FeuArtifice feux, List<Carte> carteDefausses) {

    // Vérifier si la carte est jouable immédiatement
    if (feux.estCarteJouable(carte)) {
        return EvaluationCarte.JOUABLE_IMMEDIATEMENT;
    }
    // Vérifier si la carte est dangereuse et ne peut pas être défaussée
    if (!peutDefausserCarte(carte, carteDefausses)) {
        return EvaluationCarte.DANGEREUSE;
    }
    // Vérifier si la carte est jouable plus tard
    if (feux.estCarteJouablePlusTard(carte)) {
        return EvaluationCarte.JOUABLE_PLUS_TARD;
    }
    // Vérifier si la carte est inutile
    if (feux.estCarteInutile(carte)) {
        return EvaluationCarte.INUTILE;
    }
    
    // Par défaut, retourner JESAISPAS si aucune condition n'est satisfaite
    return EvaluationCarte.JESAISPAS;
}


/**
 * Détermine si une carte peut être défaussée en fonction des règles du jeu Hanabi.
 *
 * @param carte           La carte à évaluer.
 * @param cartesDefausses La liste des cartes déjà défaussées.
 * @return true si la carte peut être défaussée, false sinon.
 */
private boolean peutDefausserCarte(Carte carte, List<Carte> cartesDefausses) {
    // Règle 1 : Ne peut pas défausser une carte de valeur 5
    if (carte.getValeur() == 5) {
        return false; // Ne peut pas défausser une carte de valeur 5
    }

    int cmpt = 0;

    // Règle 2 : Vérifier si une carte identique (même valeur et couleur) est déjà défaussée
    for (Carte carteDefaussee : cartesDefausses) {
        if (carteDefaussee.getValeur() == carte.getValeur() && carteDefaussee.getCouleur().equals(carte.getCouleur())) {
            cmpt++; // La carte avec la valeur et la couleur spécifiées est présente dans les cartes défaussées
        }
    }

    // Règle 3 : Ne peut pas défausser une troisième carte de valeur 1
    if (carte.getValeur() == 1 && cmpt == 2) {
        return false; // Ne peut pas défausser une troisième carte de valeur 1
    }

    // Règle 4 : Ne peut pas défausser une carte de valeur {4, 3, 2}
    // si une carte de cette valeur et même couleur est déjà défaussée
    if (carte.getValeur() != 1 && cmpt == 1) {
        return false; // Ne peut pas défausser une carte de valeur {4, 3, 2} si une carte de cette valeur et même couleur est déjà défaussée
    }

    // Si aucune des conditions ci-dessus n'est remplie, la carte peut être défaussée
    return true;
}



/**
 * Vérifie si une carte est inutile dans la main du joueur.
 * Retourne l'indice de la première carte inutile trouvée, sinon retourne -1.
 *
 * @param joueur         Le joueur actuel.
 * @param feux           L'état actuel des feux.
 * @param cartesDefausses Les cartes déjà défaussées.
 * @return L'indice de la carte inutile, ou -1 si aucune carte n'est inutile.
 */
public Integer aCarteInutile(Joueur joueur, FeuArtifice feux, List<Carte> cartesDefausses) {
    List<Integer> indicesCartesRevelees = joueur.getIndicesCartesrevelees();
    List<Integer> indicesCartesReveleesParValeur = joueur.getIndicesCartesreveleesParValeur();

    for (Integer indice : indicesCartesRevelees) {
        if (evaluerCarte(joueur.getMain().get(indice), feux, cartesDefausses) == EvaluationCarte.INUTILE) {
            return indice;
        }
    }

    for (Integer indice : indicesCartesReveleesParValeur) {
        if (feux.estCarteInutile(joueur.getMain().get(indice).getValeur())) {
            return indice;
        }
    }

    return -1;
}


/**
 * Choisit une carte à défausser en évaluant les cartes de la main du joueur.
 * Retourne l'indice de la première carte inutile trouvée, sinon retourne un indice aléatoire de carte non révélée.
 *
 * @param joueur         Le joueur actuel.
 * @param feux           L'état actuel des feux.
 * @param cartesDefausses Les cartes déjà défaussées.
 * @return L'indice de la carte à défausser.
 */
public int choisirCarteADefausser(Joueur joueur, FeuArtifice feux, List<Carte> cartesDefausses) {
    Integer indiceCarteInutile = aCarteInutile(joueur, feux, cartesDefausses);

    if (indiceCarteInutile != -1) {
        return indiceCarteInutile;
    }

    List<Integer> indicesCartesNonRevelees = joueur.getIndicesCartesNonrevelees();

    if (!indicesCartesNonRevelees.isEmpty()) {
        return indicesCartesNonRevelees.get(new Random().nextInt(indicesCartesNonRevelees.size()));
    }

    return -1;
}
/**
 * Vérifie si le joueur a une carte jouable immédiatement dans sa main.
 * @param joueur Le joueur dont on examine la main.
 * @param feux L'état actuel des feux d'artifice.
 * @param cartesDefausses La liste des cartes déjà défaussées.
 * @return L'indice de la première carte jouable immédiatement dans la main du joueur, ou -1 si aucune carte n'est jouable.
 */
public Integer aCarteJouableImmediat(Joueur joueur, FeuArtifice feux, List<Carte> cartesDefausses) {
    List<Integer> indicesCartesRevelees = joueur.getIndicesCartesrevelees();
    List<Integer> indicesCartesReveleesParValeur = joueur.getIndicesCartesreveleesParValeur();

    // Vérifie les cartes révélées pour une jouabilité immédiate
    for (Integer indice : indicesCartesRevelees) {
        if (evaluerCarte(joueur.getMain().get(indice), feux, cartesDefausses) == EvaluationCarte.JOUABLE_IMMEDIATEMENT) {
            return indice; // Retourne l'indice de la carte jouable immédiatement
        }
    }
    // Vérifie les cartes révélées par valeur pour une jouabilité immédiate
    for (Integer indice : indicesCartesReveleesParValeur) {
        if (feux.estCarteJouable(joueur.getMain().get(indice).getValeur())) {
            return indice; // Retourne l'indice de la carte jouable immédiatement
        }
    }
    return -1; // Aucune carte jouable immédiatement
}

/**
 * Choix d'une carte à jouer pour un joueur donné, basé sur l'état actuel du jeu.
 * @param joueur Le joueur qui doit jouer une carte.
 * @param etat L'état actuel du jeu, incluant les feux d'artifice et les cartes défaussées.
 * @return L'indice de la carte que le joueur devrait jouer.
 */
public int choisirCarteAJouer(Joueur joueur, Etat etat) {
    // Appelle aCarteJouableImmediat pour déterminer si une carte peut être jouée immédiatement
    return aCarteJouableImmediat(joueur, etat.getFeux(), etat.getCartesDefausses());
}


public String donnerIndiceCouleur(Joueur autreJoueur, String couleur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices="";
        int compt = 0;
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getCouleur().equals(couleur) ) {
                    //carte.revelerCouleur();
                    compt++;
                    indices+=" " +mainAutreJoueur.indexOf(carte);

                }
            }
        }

        if(compt!=0)
            return " a " + compt + " cartes de couleur " + couleur + " à l'indice " + indices;   
        return null;
}

public String donnerIndiceValeur(Joueur autreJoueur, int valeur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices="";
        int compt = 0;
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getValeur() == valeur ) {
                    //carte.revelerValeur();
                    compt++;
                    indices+=" " +mainAutreJoueur.indexOf(carte);
                }
            }
        }
        if(compt!=0)
            return " a " + compt + " cartes de valeur " + valeur + " à l'indice " + indices;
        return null;
}

public String toString(){
     return " C'est notre Strategie";

}


}

