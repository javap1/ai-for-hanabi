package model;

import java.util.Arrays;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StrategieProbabiliste implements Strategie {

    // Constantes pour les actions possibles
    private static final int DONNER_INDICE_ACTION = 1;
    private static final int JOUER_CARTE_ACTION = 2;
    private static final int DEFAUSSER_CARTE_ACTION = 3;

    // Constantes pour les états de cartes
    private static final int JOUABLE_IMMEDIATEMENT = 0;
    private static final int CARTE_INUTILE = 1;
    private static final int JOUABLE_PLUS_TARD = 2;
    private static final int CARTE_DANGEREUSE = 3;

    // Probabilité minimale pour déterminer les actions à effectuer
    private float probabiliteMinimal;

     /**
     * Constructeur de la stratégie probabiliste.
     *
     * @param probabiliteMinimal La probabilité minimale pour déterminer les actions à effectuer.
     */
    public StrategieProbabiliste(float probabiliteMinimal) {
        this.probabiliteMinimal=probabiliteMinimal;
    }

    /**
     * Méthode pour déterminer l'action à effectuer par le joueur.
     *
     * @param joueurCourant Le joueur actuel.
     * @param etat          L'état actuel du jeu.
     * @return L'action à effectuer.
     */
    public Action actionAfaire(Joueur joueurCourant, Etat etat) {
            int choix = choisirAction(joueurCourant, etat);

            switch (choix) {
                case DONNER_INDICE_ACTION:
                    return donnerIndice(joueurCourant, etat);
                case JOUER_CARTE_ACTION:
                    return jouerCarte(joueurCourant, etat);
                case DEFAUSSER_CARTE_ACTION:
                    return defausserCarte(joueurCourant, etat);
                default:
                    throw new IllegalArgumentException("Choix invalide: " + choix);
            }
        }

/**
 * Choix de l'action à effectuer par le joueur.
 *
 * @param joueurCourant Le joueur actuel.
 * @param etat          L'état actuel du jeu.
 * @return Le choix de l'action à effectuer.
 */
private int choisirAction(Joueur joueurCourant,Etat etat ) {
    // Si une carte inutile est présente et il reste moins de 8 jetons bleus, défausser une carte
    if (aCarteInutile(joueurCourant, etat)!=-1 && etat.getJetonsBleu() < 8)
        return DEFAUSSER_CARTE_ACTION;
    // Si une carte peut être jouée immédiatement, la jouer
    if (aCarteJouableImmediat(joueurCourant, etat)!=-1) {
        return JOUER_CARTE_ACTION;

    // Si une carte jouable immédiatement pour un autre joueur existe et il reste des jetons bleus, donner un indice
    } else if (existeCarteJouableImmediatPourAutreJoueur(joueurCourant, etat) && etat.getJetonsBleu() > 0) {
        return DONNER_INDICE_ACTION;

    // Si une carte dangereuse pour un autre joueur existe et il reste des jetons bleus, donner un indice
    } else if (existeCarteDangereusePourAutreJoueur(joueurCourant, etat)  && etat.getJetonsBleu() > 0) {
        return DONNER_INDICE_ACTION;

    // Si une carte jouable plus tard pour un autre joueur existe et il reste des jetons bleus, donner un indice    
    } else if ( existeCarteJouableTardPourAutreJoueur(joueurCourant, etat) && etat.getJetonsBleu() > 0) {
        return DONNER_INDICE_ACTION;
    // Sinon, défausser une carte
    }else {
        return DEFAUSSER_CARTE_ACTION;
    }
}


/**
 * Choix de l'action à effectuer pour donner un indice au joueur.
 *
 * @param joueurCourant Le joueur actuel.
 * @param etat          L'état actuel du jeu.
 * @return L'action à effectuer pour donner un indice.
 */
public Action donnerIndice(Joueur joueurCourant, Etat etat) {
    Action action;
    System.out.println("CARTE JOUABLE IMMEDIATEMENT");
    action = donnerIndice_Carte(joueurCourant, etat,EvaluationCarte.JOUABLE_IMMEDIATEMENT , JOUABLE_IMMEDIATEMENT );
    if (action != null) 
      return action;
    System.out.println("CARTE DANGEREUSE");
    action = donnerIndice_Carte(joueurCourant, etat,EvaluationCarte.DANGEREUSE , CARTE_DANGEREUSE);
    if (action != null) 
      return action;
    System.out.println("CARTE JOUABLE PLUS TARD");
    action = donnerIndice_Carte(joueurCourant, etat,EvaluationCarte.JOUABLE_PLUS_TARD , JOUABLE_PLUS_TARD);
    if (action != null) 
      return action;
 // Aucun indice trouvé ou déjà donné, on défausse une carte
    System.out.println("Indice non trouvé ou déjà donné. On défausse une carte.");
    return defausserCarte(joueurCourant, etat);
}

/**
 * Donne un indice pour une carte évaluée spécifique.
 *
 * @param joueurCourant Le joueur actuel.
 * @param etat          L'état actuel du jeu.
 * @param evaluation     L'évaluation de la carte.
 * @param proba_souhaiter  La probabilité minimale souhaitée.
 * @return L'action à effectuer pour donner un indice.
 */
public Action donnerIndice_Carte(Joueur joueurCourant, Etat etat, EvaluationCarte evaluation, int proba_souhaiter ) {
    String indice=null;
    
    // Deuxième passage pour les cartes évaluées comme JOUABLE_PLUS_TARD
    for (Joueur autreJoueur : etat.getJoueurs()) {
        autreJoueur.montrermain(); 
        if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
            for (Carte carte : autreJoueur.getMain()) {
                // Évalue la carte
                EvaluationCarte evaluation_carte = evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses());
                
                // Vérifie si la carte est évaluée comme JOUABLE_PLUS_TARD
                if (evaluation == evaluation_carte ) {
                    float[] prob_avant = probabilite_joueur_visée(carte ,joueurCourant, etat,autreJoueur, "avant" );
                    
                    if(prob_avant[proba_souhaiter]<probabiliteMinimal){
                        if(!carte.estReveleeValeur() && !carte.estReveleeCouleur()){
                                
                            float[] prob_valeur = probabilite_joueur_visée(carte ,joueurCourant, etat,autreJoueur, "valeur" );
                            float[] prob_couleur = probabilite_joueur_visée(carte ,joueurCourant, etat,autreJoueur, "couleur" );
                            if (prob_valeur[proba_souhaiter] > prob_couleur[proba_souhaiter]) {
                            // Donne un indice de valeur pour une carte jouable plus tard
                            indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                            // Vérifie si l'indice est valide et n'a pas été donné auparavant
                            if (indice != null ) {
                                return new Action(autreJoueur.getNom(),String.valueOf(carte.getValeur()));

                            }
                        }else{
                                indice = donnerIndiceCouleur(autreJoueur, carte.getCouleur());
                                // Vérifie si l'indice n'a pas déjà été donné
                                if (indice != null ) {
                                    return new Action(autreJoueur.getNom(), carte.getCouleur());
                                }
                    }}
                    if (!carte.estReveleeValeur() && carte.estReveleeCouleur()){
                             // Vérifie si l'indice est valide et n'a pas été donné auparavant
                                indice = donnerIndiceValeur(autreJoueur, carte.getValeur());
                                // Vérifie si l'indice est valide et n'a pas été donné auparavant
                                if (indice != null ) {
                                    return new Action(autreJoueur.getNom(), String.valueOf(carte.getValeur()));
                                }
                            }
                        
                    if(!carte.estReveleeCouleur() && carte.estReveleeValeur()){
                                // Donne un indice de couleur si l'indice de valeur n'est pas possible
                            
                            
                                indice = donnerIndiceCouleur(autreJoueur, carte.getCouleur());
                                      // Vérifie si l'indice de couleur est valide et n'a pas été donné auparavant
                                if (indice != null ) {
                                        return new Action(autreJoueur.getNom(), carte.getCouleur());
                                    }
                        
                        }
                        
                }
                }
            }
        }
    }
return null;
   
}

  
/**
 * Sélectionne une carte à jouer pour le joueur courant et crée une action correspondante.
 * @param joueurCourant Le joueur qui doit jouer une carte.
 * @param etat L'état actuel du jeu.
 * @return Une action représentant la carte jouée.
 */
public Action jouerCarte(Joueur joueurCourant, Etat etat) {
    // Affiche la main actuelle du joueur avec les probabilités d'évaluation pour chaque carte
    System.out.println("Observation de la main du joueur courant " + joueurCourant.getNom() + " : ");
     // Choisis une carte à jouer en se basant sur l'état actuel du jeu
    int indiceCarteAJouer = aCarteJouableImmediat(joueurCourant, etat);
    Carte carte = joueurCourant.getMain().get(indiceCarteAJouer);
    float[] probabilites = probabiliteEvaluation(carte, joueurCourant, etat);
    System.out.println("Carte : " + carte.toString());
    System.out.println("Probabilité de jouable immédiatement : " + probabilites[0]);
    // Retourne une action pour jouer la carte sélectionnée
    return new Action(indiceCarteAJouer); 
}


/**
 * Sélectionne une carte à défausser pour le joueur courant et crée une action correspondante.
 * @param joueurCourant Le joueur qui doit défausser une carte.
 * @param etat L'état actuel du jeu.
 * @return Une action représentant la carte défaussée.
 */
public Action defausserCarte(Joueur joueurCourant, Etat etat) {
    // Affiche la main actuelle du joueur
    System.out.println("Observation de la main du joueur courant " + joueurCourant.getNom() + " : ");
        // Choisis une carte à défausser en se basant sur l'état actuel du jeu
    int indiceCarteAdefausser = aCarteInutile(joueurCourant, etat);
    Carte carte ;
    float prob=0;
    if(indiceCarteAdefausser == -1){
        for (int i=0 ; i<joueurCourant.getMain().size();i++) {
            carte=joueurCourant.getMain().get(i);
            float[] probabilites = probabiliteEvaluation(carte, joueurCourant, etat);
            if(prob < probabilites[CARTE_INUTILE]){
                indiceCarteAdefausser= i;
                prob=probabilites[1];
            }
            System.out.println("Carte : " + carte.toString());
            System.out.println("Probabilité d'être inutile : " + probabilites[1]);
            System.out.println(); // Ajout d'une ligne vide pour séparer les informations de chaque carte
        }
    
    }
    // Si aucune carte n'est jugée défaussable, en choisir une au hasard
    if (indiceCarteAdefausser == -1) {
        indiceCarteAdefausser = new Random().nextInt(joueurCourant.getMain().size()); // Remplacer 4 par la taille de la main si différente
    }

    // Retourne une action pour défausser la carte sélectionnée
    return new Action(3, indiceCarteAdefausser); // Le chiffre 3 pourrait représenter un code d'action spécifique
}

    

    
/**
 * Vérifie si un joueur autre que le joueur courant a une carte jouable immédiatement dans sa main.
 * @param joueurCourant Le joueur actuellement en train de jouer.
 * @param etat L'état actuel du jeu, incluant les joueurs, les feux d'artifice et les cartes défaussées.
 * @return true si un autre joueur a une carte jouable immédiatement, false sinon.
 */
public boolean existeCarteJouableImmediatPourAutreJoueur(Joueur joueurCourant, Etat etat) {
    // Vérifier pour chaque joueur (sauf le joueur courant) si une carte jouable immédiatement existe
    return etat.getJoueurs().stream()
            .filter(joueur -> !joueur.getNom().equals(joueurCourant.getNom()))
            .flatMap(joueur -> joueur.getMain().stream())
            .anyMatch(carte -> evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses()) == EvaluationCarte.JOUABLE_IMMEDIATEMENT);
}

/**
 * Vérifie si un joueur autre que le joueur courant a une carte jouable plus tard dans sa main.
 * @param joueurCourant Le joueur actuellement en train de jouer.
 * @param etat L'état actuel du jeu, incluant les joueurs, les feux d'artifice et les cartes défaussées.
 * @return true si un autre joueur a une carte jouable plus tard, false sinon.
 */
public boolean existeCarteJouableTardPourAutreJoueur(Joueur joueurCourant, Etat etat) {
    // Vérifier pour chaque joueur (sauf le joueur courant) si une carte jouable plus tard existe
    return etat.getJoueurs().stream()
            .filter(joueur -> !joueur.getNom().equals(joueurCourant.getNom()))
            .flatMap(joueur -> joueur.getMain().stream())
            .anyMatch(carte -> evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses()) == EvaluationCarte.JOUABLE_PLUS_TARD);
}

/**
 * Vérifie si un joueur autre que le joueur courant a une carte dangereuse dans sa main.
 * @param joueurCourant Le joueur actuellement en train de jouer.
 * @param etat L'état actuel du jeu, incluant les joueurs, les feux d'artifice et les cartes défaussées.
 * @return true si un autre joueur a une carte dangereuse, false sinon.
 */
public boolean existeCarteDangereusePourAutreJoueur(Joueur joueurCourant, Etat etat) {
    // Vérifier pour chaque joueur (sauf le joueur courant) si une carte dangereuse existe
    return etat.getJoueurs().stream()
            .filter(joueur -> !joueur.getNom().equals(joueurCourant.getNom()))
            .flatMap(joueur -> joueur.getMain().stream())
            .anyMatch(carte -> evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses()) == EvaluationCarte.DANGEREUSE);
}

/**
 * Enumération pour les différents types d'évaluation d'une carte.
 */
public enum EvaluationCarte {
    JOUABLE_IMMEDIATEMENT, // La carte peut être jouée immédiatement
    INUTILE,              // La carte est inutile et ne peut pas contribuer au jeu
    JOUABLE_PLUS_TARD,    // La carte peut être jouée mais à un moment ultérieur
    DANGEREUSE,           // La carte est dangereuse et son jeu peut être risqué
    JESAISPAS             // Incertitude sur la manière d'utiliser la carte
}

/**
 * Évalue la carte en fonction de sa jouabilité et de sa pertinence dans le jeu actuel.
 * @param carte La carte à évaluer.
 * @param feux L'état actuel des feux d'artifice.
 * @param carteDefausses La liste des cartes déjà défaussées.
 * @return L'évaluation de la carte en fonction des règles du jeu.
 */
public EvaluationCarte evaluerCarte(Carte carte, FeuArtifice feux, List<Carte> carteDefausses) {
    // Vérifier si la carte est dangereuse et ne peut pas être défaussée
    
    // Vérifier si la carte est jouable immédiatement
    if (feux.estCarteJouable(carte)) {
        return EvaluationCarte.JOUABLE_IMMEDIATEMENT;
    }
    if (!peutDefausserCarte(carte, carteDefausses)) {
        return EvaluationCarte.DANGEREUSE;
    }
    // Vérifier si la carte est inutile
    
    // Vérifier si la carte est jouable plus tard
    if (feux.estCarteJouablePlusTard(carte)) {
        return EvaluationCarte.JOUABLE_PLUS_TARD;
    }
    if (feux.estCarteInutile(carte)) {
        return EvaluationCarte.INUTILE;
    }
    // Par défaut, retourner JESAISPAS si aucune condition n'est satisfaite
    return EvaluationCarte.JESAISPAS;
}


/**
 * Détermine si une carte peut être défaussée en fonction des règles du jeu Hanabi.
 *
 * @param carte           La carte à évaluer.
 * @param cartesDefausses La liste des cartes déjà défaussées.
 * @return true si la carte peut être défaussée, false sinon.
 */
private boolean peutDefausserCarte(Carte carte, List<Carte> cartesDefausses) {
    // Règle 1 : Ne peut pas défausser une carte de valeur 5
    if (carte.getValeur() == 5) {
        return false; // Ne peut pas défausser une carte de valeur 5
    }

    int cmpt = 0;

    // Règle 2 : Vérifier si une carte identique (même valeur et couleur) est déjà défaussée
    for (Carte carteDefaussee : cartesDefausses) {
        if (carteDefaussee.getValeur() == carte.getValeur() && carteDefaussee.getCouleur().equals(carte.getCouleur())) {
            cmpt++; // La carte avec la valeur et la couleur spécifiées est présente dans les cartes défaussées
        }
    }

    // Règle 3 : Ne peut pas défausser une troisième carte de valeur 1
    if (carte.getValeur() == 1 && cmpt == 2) {
        return false; // Ne peut pas défausser une troisième carte de valeur 1
    }

    // Règle 4 : Ne peut pas défausser une carte de valeur {4, 3, 2}
    // si une carte de cette valeur et même couleur est déjà défaussée
    if (carte.getValeur() != 1 && cmpt == 1) {
        return false; // Ne peut pas défausser une carte de valeur {4, 3, 2} si une carte de cette valeur et même couleur est déjà défaussée
    }

    // Si aucune des conditions ci-dessus n'est remplie, la carte peut être défaussée
    return true;
}



/**
 * Vérifie si une carte est inutile dans la main du joueur.
 * Retourne l'indice de la première carte inutile trouvée, sinon retourne -1.
 *
 * @param joueur         Le joueur actuel.
 * @param feux           L'état actuel des feux.
 * @param cartesDefausses Les cartes déjà défaussées.
 * @return L'indice de la carte inutile, ou -1 si aucune carte n'est inutile.
 */
public Integer aCarteInutile(Joueur joueur, Etat etat) {
    // Initialiser l'indice de la carte inutile et la probabilité maximale
    Integer indiceCarteInutile = -1;
    float probaMaxInutile = probabiliteMinimal;

    // Parcourir toutes les cartes de la main du joueur
    for (int i = 0; i < joueur.getMain().size(); i++) {
        Carte carte = joueur.getMain().get(i);

        // Calculer les probabilités d'évaluation pour la carte
        float[] probabilites = probabiliteEvaluation(carte, joueur, etat);

        // La probabilité d'être inutile est la deuxième valeur du tableau de probabilités
        float probaInutile = probabilites[CARTE_INUTILE];

        // Mettre à jour l'indice de la carte inutile et la probabilité maximale si nécessaire
        if (probaInutile > probaMaxInutile) {
            probaMaxInutile = probaInutile;
            indiceCarteInutile = i;
        }
    }

    return indiceCarteInutile;
}




/**
 * Vérifie si le joueur a une carte jouable immédiatement dans sa main.
 * @param joueur Le joueur dont on examine la main.
 * @param feux L'état actuel des feux d'artifice.
 * @param cartesDefausses La liste des cartes déjà défaussées.
 * @return L'indice de la première carte jouable immédiatement dans la main du joueur, ou -1 si aucune carte n'est jouable.
 */
public Integer aCarteJouableImmediat(Joueur joueur, Etat etat) {
    // Initialiser l'indice de la carte jouable immédiatement et la probabilité maximale
    Integer indiceCarteJouable = -1;
    float probaMaxJouable = probabiliteMinimal;

    // Parcourir toutes les cartes de la main du joueur
    for (int i = 0; i < joueur.getMain().size(); i++) {
        Carte carte = joueur.getMain().get(i);

        // Calculer les probabilités d'évaluation pour la carte
        float[] probabilites = probabiliteEvaluation(carte, joueur, etat);

        // La probabilité d'être jouable immédiatement est la première valeur du tableau de probabilités
        float probaJouable = probabilites[0];

        // Mettre à jour l'indice de la carte jouable immédiatement et la probabilité maximale si nécessaire
        if (probaJouable > probaMaxJouable) {
            probaMaxJouable = probaJouable;
            indiceCarteJouable = i;
        }
    }

    return indiceCarteJouable;
}



/**
 * Donne un indice de couleur à un autre joueur en indiquant combien de cartes de cette couleur il a dans sa main.
 * @param autreJoueur Le joueur à qui donner l'indice.
 * @param couleur La couleur à indiquer.
 * @return Une chaîne de caractères indiquant combien de cartes de la couleur donnée le joueur possède et à quels indices,
 *         ou null si aucune carte de cette couleur n'est trouvée.
 */

public String donnerIndiceCouleur(Joueur autreJoueur, String couleur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices="";
        int compt = 0;
        // Vérifie si la main de l'autre joueur n'est pas vide

        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                if (carte.getCouleur().equals(couleur) ) {
                    //carte.revelerCouleur();
                    compt++;
                    indices+=" " +mainAutreJoueur.indexOf(carte);

                }
            }
        }

        if(compt!=0)
            return " a " + compt + " cartes de couleur " + couleur + " à l'indice " + indices;   
        return null;
}
/**
 * Donne un indice de valeur à un autre joueur en indiquant combien de cartes de cette valeur il a dans sa main.
 * @param autreJoueur Le joueur à qui donner l'indice.
 * @param valeur La valeur à indiquer.
 * @return Une chaîne de caractères indiquant combien de cartes de la valeur donnée le joueur possède et à quels indices,
 *         ou null si aucune carte de cette valeur n'est trouvée.
 */

public String donnerIndiceValeur(Joueur autreJoueur, int valeur) {
        List<Carte> mainAutreJoueur = autreJoueur.getMain();
        String indices="";
        int compt = 0;
        // Vérifie si la main de l'autre joueur n'est pas vide
        if (!mainAutreJoueur.isEmpty()) {
            for (Carte carte : mainAutreJoueur) {
                 // Si la valeur de la carte correspond à la valeur donnée
                if (carte.getValeur() == valeur ) {
                    
                    compt++;
                    indices+=" " +mainAutreJoueur.indexOf(carte);// Ajoute l'index de la carte aux indices
                }
            }
        }
        // Si au moins une carte correspond à la valeur donnée, retourne le message d'indice
        if(compt!=0)
            return " a " + compt + " cartes de valeur " + valeur + " à l'indice " + indices;
        return null;// Si aucune carte ne correspond, retourne null
}

public String toString(){
     return " C'est la Strategie Probabiliste avec probabilité minimal = "+ probabiliteMinimal;

}

/**
 * Génère une liste de toutes les cartes possibles dans le jeu avec leurs couleurs et valeurs prédéfinies.
 * @return Une liste de toutes les cartes possibles.
 */
public static List<Carte> toutesLesCartes() {
        List<Carte> cartes = new ArrayList<>();
        String[] couleurs = { "Rouge", "Bleu", "Vert", "Jaune", "Blanc" };
        int[] valeurs = { 1, 1, 1, 2, 2, 3, 3, 4, 4, 5 };
        
        for (String couleur : couleurs) {
            for (int valeur : valeurs) {
                cartes.add(new Carte(couleur, valeur));
            }
        }
        
        return cartes;
}

/**
 * Calcule les cartes possibles que le joueur courant pourrait avoir dans sa main en fonction de l'état du jeu.
 * @param joueurCourant Le joueur courant dont on calcule les cartes possibles.
 * @param etat L'état actuel du jeu.
 * @return Une liste de cartes possibles que le joueur courant pourrait avoir.
 */
public List<Carte> cartesPossibles(Joueur joueurCourant, Etat etat) {
    List<Carte> cartesPossibles= toutesLesCartes() ;

    // Enlever les cartes de la main des autres joueurs
    for (Joueur autreJoueur : etat.getJoueurs()) {
        if (!autreJoueur.getNom().equals(joueurCourant.getNom())) {
            for (Carte carte : autreJoueur.getMain()) {
                Iterator<Carte> it = cartesPossibles.iterator();
                while (it.hasNext()) {
                    Carte possible = it.next();
                    if (possible.getCouleur().equals(carte.getCouleur()) && possible.getValeur() == carte.getValeur()) {
                        it.remove(); // Utilisation de l'itérateur pour supprimer en toute sécurité
                        break; // Sortir de la boucle interne après avoir supprimé une carte
                    }
                }
            }
        }
    }

    // Enlever les cartes dejà jouées
    for (List<Carte> pile : etat.getFeux().getPiles().values()) {
        for (Carte carte : pile) {
            Iterator<Carte> it = cartesPossibles.iterator();
            while (it.hasNext()) {
                Carte possible = it.next();
                if (possible.getCouleur().equals(carte.getCouleur()) && possible.getValeur() == carte.getValeur()) {
                    it.remove(); // Utilisation de l'itérateur pour supprimer en toute sécurité
                    break; // Sortir de la boucle interne après avoir supprimé une carte
                }
            }
        }
    }


     // Enlever les cartes déjà défaussées
    for (Carte carteDefaussee : etat.getCartesDefausses()) {
        Iterator<Carte> it = cartesPossibles.iterator();
        while (it.hasNext()) {
            Carte possible = it.next();
            if (possible.getCouleur().equals(carteDefaussee.getCouleur()) && possible.getValeur() == carteDefaussee.getValeur()) {
                it.remove(); // Utilisation de l'itérateur pour supprimer en toute sécurité
                break;
            }
        }
    }


    // Enlever les cartes révélées dans la main du joueur courant
    for (Carte carteMain : joueurCourant.getMain()) {
        if (carteMain.estReveleeCouleur() && carteMain.estReveleeValeur()) {
            Iterator<Carte> it = cartesPossibles.iterator();
            while (it.hasNext()) {
                Carte possible = it.next();
                if (possible.getCouleur().equals(carteMain.getCouleur()) && possible.getValeur() == carteMain.getValeur()) {
                    it.remove(); // Utilisation de l'itérateur pour supprimer en toute sécurité
                    break; // Sortir de la boucle après avoir supprimé une carte
                }
            }
        }
    }

    return cartesPossibles;

}
/**
 * Évalue la probabilité d'une carte donnée dans la main du joueur courant en fonction de son état dans le jeu.
 * @param carte La carte à évaluer.
 * @param joueurCourant Le joueur courant possédant la carte.
 * @param etat L'état actuel du jeu.
 * @return Un tableau de probabilités indiquant la probabilité que la carte soit (dans l'ordre) jouable immédiatement, inutile, jouable plus tard, ou dangereuse.
 */
public float[] probabiliteEvaluation(Carte carte , Joueur joueurCourant, Etat etat ){
    float[] probabiliteEvaluation = new float[4]; //[0] Jouable Immediat ,[1] Inutile , [2] Jouable PLus tard ,[3] Dangereuse
    
    // Initialiser le tableau avec des zéros
    Arrays.fill(probabiliteEvaluation, 0.0f);

    // Vérifier si la carte est dans la main du joueur
    if(!joueurCourant.getMain().contains(carte)){
        System.out.println("carte n'appartient pas à la main du joueur");
    }
    else{
        // Si la carte est révélée en couleur et en valeur
        if(carte.estReveleeCouleur() && carte.estReveleeValeur()){
            EvaluationCarte eval = evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses());
            switch (eval) {
                case JOUABLE_IMMEDIATEMENT:
                    probabiliteEvaluation[0] = 1.0f; // La carte est jouable immédiatement avec certitude
                    break;
                case INUTILE:
                    probabiliteEvaluation[1] = 1.0f; // La carte est inutile avec certitude
                    break;
                case JOUABLE_PLUS_TARD:
                    probabiliteEvaluation[2] = 1.0f; // La carte est jouable plus tard avec certitude
                    break;
                case DANGEREUSE:
                    probabiliteEvaluation[3] = 1.0f; // La carte est dangereuse avec certitude
                    break;
                default:
                    break;
            }
        }
        // Si la carte n'est pas révélée en couleur ou en valeur
        else {
            List<Carte> cartesPossibles = cartesPossibles(joueurCourant, etat);


                    // Supprimer les cartes qui n'ont pas la même couleur que la carte
            if (carte.estReveleeCouleur() && !carte.estReveleeValeur()) {
                cartesPossibles.removeIf(possible -> !possible.getCouleur().equals(carte.getCouleur()));
            }
            
            // Supprimer les cartes qui n'ont pas la même valeur que la carte
            if (!carte.estReveleeCouleur() && carte.estReveleeValeur()) {
                cartesPossibles.removeIf(possible -> possible.getValeur() != carte.getValeur());
            }
            int totalCartes = cartesPossibles.size();
            for (Carte possible : cartesPossibles) {
                EvaluationCarte eval = evaluerCarte(possible, etat.getFeux(), etat.getCartesDefausses());
                switch (eval) {
                    case JOUABLE_IMMEDIATEMENT:
                        probabiliteEvaluation[0] += 1.0f / totalCartes; // Ajoute la probabilité de jouable immédiatement
                        break;
                    case INUTILE:
                        probabiliteEvaluation[1] += 1.0f / totalCartes; // Ajoute la probabilité d'être inutile
                        break;
                    case JOUABLE_PLUS_TARD:
                        probabiliteEvaluation[2] += 1.0f / totalCartes; // Ajoute la probabilité de jouable plus tard
                        break;
                    case DANGEREUSE:
                        probabiliteEvaluation[3] += 1.0f / totalCartes; // Ajoute la probabilité d'être dangereuse
                        break;
                    default:
                        break;
                }
            }
        }
    }
    return probabiliteEvaluation;
}

/**
 * Évalue la probabilité d'une carte donnée dans la main d'un joueur visé en fonction de l'état du jeu et de l'information révélée.
 * @param carte La carte à évaluer.
 * @param joueurCourant Le joueur courant.
 * @param etat L'état actuel du jeu.
 * @param visée Le joueur visé dont la main est analysée.
 * @param revelée L'information révélée ("couleur" ou "valeur").
 * @return Un tableau de probabilités indiquant la probabilité que la carte soit (dans l'ordre) jouable immédiatement, inutile, jouable plus tard, ou dangereuse.
 */
public float[] probabilite_joueur_visée(Carte carte , Joueur joueurCourant, Etat etat,Joueur visée,String revelée ){
    float[] probabiliteEvaluation = new float[4]; //[0] Jouable Immediat ,[1] Inutile , [2] Jouable PLus tard ,[3] Dangereuse
    
    // Initialiser le tableau avec des zéros
    Arrays.fill(probabiliteEvaluation, 0.0f);

    // Vérifier si la carte est dans la main du joueur
    if(!visée.getMain().contains(carte)){
        System.out.println("carte n'appartient pas à la main du joueur");
    }
    else{
        // Si la carte est révélée en couleur et en valeur
            if(carte.estReveleeCouleur() && carte.estReveleeValeur()){
                EvaluationCarte eval = evaluerCarte(carte, etat.getFeux(), etat.getCartesDefausses());
                switch (eval) {
                    case JOUABLE_IMMEDIATEMENT:
                        probabiliteEvaluation[0] = 1.0f; // La carte est jouable immédiatement avec certitude
                        break;
                    case INUTILE:
                        probabiliteEvaluation[1] = 1.0f; // La carte est inutile avec certitude
                        break;
                    case JOUABLE_PLUS_TARD:
                        probabiliteEvaluation[2] = 1.0f; // La carte est jouable plus tard avec certitude
                        break;
                    case DANGEREUSE:
                        probabiliteEvaluation[3] = 1.0f; // La carte est dangereuse avec certitude
                        break;
                    default:
                        break;
                }
            }
        // Si la carte n'est pas révélée en couleur ou en valeur
            else {
                List<Carte> cartesPossibles = cartesPossibles(joueurCourant, etat);
                    
                     // Ajouter les cartes non révélées de la main du joueur visé aux cartes possibles
                    for (Carte carte_visée : visée.getMain()) {
                        if(!carte_visée.estReveleeValeur() || ! carte_visée.estReveleeCouleur()){
                           cartesPossibles.add((Carte)carte_visée.clone());
                        }
                    }
                    // Filtrer les cartes possibles en fonction de l'information révélée
                    if(!carte.estReveleeCouleur() && !carte.estReveleeValeur()){
                        
                        if(revelée.equals("valeur")){
                           cartesPossibles.removeIf(possible -> possible.getValeur() != carte.getValeur());
                        }
                        if(revelée.equals("couleur")){
                        cartesPossibles.removeIf(possible -> !possible.getCouleur().equals(carte.getCouleur()));  
                        }
                    }
                    // Supprimer les cartes qui n'ont pas la même couleur que la carte
                    if (carte.estReveleeCouleur() && !carte.estReveleeValeur()) {
                        cartesPossibles.removeIf(possible -> !possible.getCouleur().equals(carte.getCouleur()));
                    }
                    
                    // Supprimer les cartes qui n'ont pas la même valeur que la carte
                    if (!carte.estReveleeCouleur() && carte.estReveleeValeur()) {
                        cartesPossibles.removeIf(possible -> possible.getValeur() != carte.getValeur());
                    }

        
                            
                
                int totalCartes = cartesPossibles.size();
                for (Carte possible : cartesPossibles) {
                        EvaluationCarte eval = evaluerCarte(possible, etat.getFeux(), etat.getCartesDefausses());
                        switch (eval) {
                            case JOUABLE_IMMEDIATEMENT:
                                probabiliteEvaluation[0] += 1.0f / totalCartes; // Ajoute la probabilité de jouable immédiatement
                                break;
                            case INUTILE:
                                probabiliteEvaluation[1] += 1.0f / totalCartes; // Ajoute la probabilité d'être inutile
                                break;
                            case JOUABLE_PLUS_TARD:
                                probabiliteEvaluation[2] += 1.0f / totalCartes; // Ajoute la probabilité de jouable plus tard
                                break;
                            case DANGEREUSE:
                                probabiliteEvaluation[3] += 1.0f / totalCartes; // Ajoute la probabilité d'être dangereuse
                                break;
                            default:
                                break;
                        }
                }
            }
        }
    return probabiliteEvaluation;
}

/**
 * Affiche les probabilités de l'état d'une carte en termes de jouabilité, inutilité, jouabilité future et dangerosité.
 * @param probabilite Un tableau de probabilités indiquant la probabilité que la carte soit (dans l'ordre) jouable immédiatement, inutile, jouable plus tard, ou dangereuse.
 */
public void affichage_Probabilite(float[] probabilite){
        
        System.out.println("Probabilité de jouable immédiatement : " + probabilite[0]);
        System.out.println("Probabilité d'être inutile : " + probabilite[1]);
        System.out.println("Probabilité de jouable plus tard : " + probabilite[2]);
        System.out.println("Probabilité d'être dangereuse : " + probabilite[3]);
        System.out.println();
}
}
