package model;

import java.util.*;
import java.io.PrintWriter;
import java.io.FileNotFoundException;

public class Test {
    public static void main(String[] args) {
        List<Strategie> strategieProba = new ArrayList<>();
        strategieProba.add(new StrategieProbabiliste(0.8f));
        strategieProba.add(new StrategieProbabiliste(0.8f));
        strategieProba.add(new StrategieProbabiliste(0.8f));
        List<Strategie> strategiesMax = new ArrayList<>();
        strategiesMax.add(new ScoreMax());
        strategiesMax.add(new ScoreMax());
        strategiesMax.add(new ScoreMax());
        List<Strategie> strategiesStati = new ArrayList<>();
        strategiesStati.add(new StrategieStatistique());
        strategiesStati.add(new StrategieStatistique());
        strategiesStati.add(new StrategieStatistique());
        List<Strategie> strategiesObser = new ArrayList<>();
        strategiesObser.add(new StrategieObservatrice());
        strategiesObser.add(new StrategieObservatrice());
        strategiesObser.add(new StrategieObservatrice());
        List<Strategie> strategiesAlea = new ArrayList<>();
        strategiesAlea.add(new StrategieAleatoire());
        strategiesAlea.add(new StrategieAleatoire());
        strategiesAlea.add(new StrategieAleatoire());

        Jeu jeu_Proba = new Jeu(3);
        Jeu jeu_Max = new Jeu(3);
        Jeu jeu_Stati = new Jeu(3);
        Jeu jeu_Observ = new Jeu(3);
        Jeu jeu_Alea = new Jeu(3);
        jeu_Max.remplacerEtat(jeu_Proba.getEtat().copy());
        jeu_Stati.remplacerEtat(jeu_Proba.getEtat().copy());
        jeu_Observ.remplacerEtat(jeu_Proba.getEtat().copy());
        jeu_Alea.remplacerEtat(jeu_Proba.getEtat().copy());

        try (PrintWriter writer = new PrintWriter("StrategieProbabiliste_Observatrice.txt")) {
            writer.println("100 essais avec 3 joueurs utilisant StrategieProbabiliste avec 0.20 de probabilité. Scores :");
            for (int i = 0; i < 100; i++) {
                jeu_Proba.jouer(strategieProba);
                jeu_Max.jouer(strategiesMax);
                jeu_Stati.jouer(strategiesStati);
                jeu_Observ.jouer(strategiesObser);
                jeu_Alea.jouer(strategiesAlea);

                int scorePro = jeu_Proba.getScore();
                int scoreMax = jeu_Max.getScore();
                int scoreStati = jeu_Stati.getScore();
                int scoreObserv = jeu_Observ.getScore();
                int scoreAlea = jeu_Alea.getScore();
                writer.println("Score Probabiliste :" + scorePro);
                writer.println("Score Max :" + scoreMax); // Écrire le score dans le fichier
                writer.println("Score Statistique :" + scoreStati);
                writer.println("Score Observatrice :" + scoreObserv);
                writer.println("Score Aleatoire :" + scoreAlea);
                jeu_Proba.setEtatAzero();
                jeu_Max.setEtatAzero();
                jeu_Stati.setEtatAzero();
                jeu_Observ.setEtatAzero();
                jeu_Alea.setEtatAzero();

                jeu_Max.remplacerEtat(jeu_Proba.getEtat().copy());
                jeu_Stati.remplacerEtat(jeu_Proba.getEtat().copy());
                jeu_Observ.remplacerEtat(jeu_Proba.getEtat().copy());
                jeu_Alea.remplacerEtat(jeu_Proba.getEtat().copy());
                
            }
        } catch (FileNotFoundException e) {
            System.err.println("Erreur : Impossible de créer ou d'écrire dans le fichier");
            e.printStackTrace();
        }
    }
}
