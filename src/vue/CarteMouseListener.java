package vue;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import model.*;
public class CarteMouseListener extends MouseAdapter {
    
    private ObservableCarteMouseListener observable;


    public void setObservable(ObservableCarteMouseListener observable) {
        this.observable = observable;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Obtenir la source de l'événement
        Object source = e.getSource();
        
        // Vérifier si la source est une VueCarte
        if (source instanceof VueCarte) {
            VueCarte vueCarteCliquee = (VueCarte) source;
            
            // Récupérer la carte et le nom du joueur
            Carte carteCliquee = vueCarteCliquee.getCarte();
            String joueurNom = vueCarteCliquee.getJoueurNom();
            
            // Vérifier si la carte ou le nom du joueur est null
            if (carteCliquee != null && joueurNom != null) {
                // Notifier les observateurs du changement avec des arguments
                observable.setCarteJoueurCliquee(carteCliquee,joueurNom);
                // Ajoutez ici le comportement que vous souhaitez exécuter lorsque la carte est cliquée
            } else {
                System.out.println("La carte cliquée ou le nom du joueur est null.");
            }
        }
    }

}
