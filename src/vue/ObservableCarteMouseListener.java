package vue;

import java.util.ArrayList;
import java.util.List;
import model.*;

public class ObservableCarteMouseListener implements Observable {
    private List<Observer> observers;
    private boolean changed;
    private Carte carteCliquee;
    private String joueurNom;

    public ObservableCarteMouseListener() {
        observers = new ArrayList<>();
        changed = false;
    }

    @Override
    public void addObserver(Observer o) {
        if (o == null) throw new NullPointerException();
        if (!observers.contains(o)) {
            observers.add(o);
        }
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        if (!changed) return;
        for (Observer observer : observers) {
            observer.update(this, null);
        }
        clearChanged();
    }

    @Override
    public void setChanged() {
        changed = true;
    }

    private void clearChanged() {
        changed = false;
    }

    public void setCarteJoueurCliquee(Carte carteCliquee, String joueurNom) {
        this.joueurNom = joueurNom;
        this.carteCliquee = carteCliquee;
        setChanged();
        notifyObservers();
    }

    public void setJoueurNom(String joueurNom) {
        this.joueurNom = joueurNom;
        setChanged();
        notifyObservers();
    }

    public Carte getCarteCliquee() {
        return carteCliquee;
    }

    public String getJoueurNom() {
        return joueurNom;
    }
}
