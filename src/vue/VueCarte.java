package vue;

import model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.border.BevelBorder;



public class VueCarte extends JPanel implements Observer,MouseListener {

    private final Carte carte;
    private String nomJoueur;
    private int nbMiseAjour ;

    public VueCarte(Carte carte,String nomJoueur) {
        this.carte = carte;
        this.nomJoueur=nomJoueur;
        nbMiseAjour=0;
        setPreferredSize(new Dimension(50, 70)); // Taille préférée pour chaque carte
        Color mauve = new Color(176, 96, 176);
        // Configuration de l'apparence de la carte
        setBackground(Color.GRAY);
        setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
        carte.addObserver(this); // Ajouter cette vue comme observateur de la carte
        addMouseListener(this);
    }

    public Carte getCarte(){
        return carte;
    }

    public String getJoueurNom(){
        return nomJoueur;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Vérifier si la couleur et la valeur sont révélées
        if (carte.estReveleeCouleur() && carte.estReveleeValeur()) {
            // Dessiner le recto de la carte avec la couleur et la valeur
            Color couleur = getColorFromString(carte.getCouleur());
            g.setColor(couleur);
            drawFireworkSymbols(g, 35, 25,couleur,2);  // Dessiner le symbole de flamme
            drawFireworkSymbols(g,10 , 50,couleur,1); 
            g.setColor(Color.BLACK);
            g.setFont(new Font("Arial", Font.BOLD, 14));
            g.drawString(String.valueOf(carte.getValeur()), 5, 15);
            g.drawString(String.valueOf(carte.getValeur()), 38, 65);
        } else if (carte.estReveleeCouleur()) {
            // Dessiner le recto de la carte avec seulement la couleur
            Color couleur = getColorFromString(carte.getCouleur());
            g.setColor(couleur);
            drawFireworkSymbols(g, 35, 25,couleur,2);
            drawFireworkSymbols(g,10 , 50,couleur,1);  // Dessiner le symbole de flamme
        } else if (carte.estReveleeValeur()) {
            // Dessiner le recto de la carte avec seulement la valeur
            g.setColor(Color.BLACK);
            g.setFont(new Font("Arial", Font.BOLD, 14));
            g.drawString(String.valueOf(carte.getValeur()), 5, 15);
            g.drawString(String.valueOf(carte.getValeur()), 38, 65);
        } else {
            // Dessiner le verso de la carte en noir
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    private Color getColorFromString(String couleur) {
        switch (couleur.toLowerCase()) {
            case "rouge":
                return Color.RED;
            case "jaune":
                return Color.YELLOW;
            case "bleu":
                return Color.BLUE;
            case "vert":
                return Color.GREEN;
            case "blanc":
                return Color.WHITE;
            default:
                return Color.BLACK;
        }
    }
    private void drawFireworkSymbols(Graphics g, int centerX, int centerY,Color couleur,int rayLength ) {
        int symbolSize = 5; // Taille du symbole de feu d'artifice
        int numSymbols = 8; // Nombre de symboles à dessiner

        // Calculer le rayon du cercle intérieur où les symboles seront dessinés
        int innerCircleRadius = symbolSize * 2;

        // Calculer l'angle entre chaque symbole
        double angleIncrement = Math.toRadians(360.0 / numSymbols);

        for (int i = 0; i < numSymbols; i++) {
            // Calculer l'angle pour le symbole actuel
            double angle = angleIncrement * i;

            // Calculer les coordonnées polaires du symbole par rapport au centre du cercle
            int symbolX = (int) (centerX + innerCircleRadius * Math.cos(angle));
            int symbolY = (int) (centerY + innerCircleRadius * Math.sin(angle));

            // Dessiner le symbole de feu d'artifice à la position calculée
            drawFireworkSymbol(g, symbolX, symbolY,couleur,rayLength);
        }
    }

    private void drawFireworkSymbol(Graphics g, int x, int y,Color couleur,int rayLength ) {
        Graphics2D g2d = (Graphics2D) g;

        // Définir le nombre de rayons et leur longueur
        int numRays = 8;

        // Dessiner les rayons
        for (int i = 0; i < numRays; i++) {
            double angle = Math.toRadians(360.0 / numRays * i);
            int endX = (int) (x + rayLength * Math.cos(angle));
            int endY = (int) (y + rayLength * Math.sin(angle));
            g2d.setColor(couleur);
            g2d.setStroke(new BasicStroke(3));
            g2d.drawLine(x, y, endX, endY);
        }
    }


    @Override
    public void mouseClicked(MouseEvent e) {
            // Pas besoin de cette méthode pour l'instant
        }

    @Override
    public void mousePressed(MouseEvent e) {
        // Pas besoin de cette méthode pour l'instant
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Pas besoin de cette méthode pour l'instant
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Pas besoin de cette méthode pour l'instant
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Pas besoin de cette méthode pour l'instant
    }

    
    public void update(Observable o, Object arg) {
        // Mettre à jour l'affichage en réponse aux changements d'état de la carte
        if (o instanceof Carte) {
            nbMiseAjour++;
            Border greenLineBorder = BorderFactory.createLineBorder(Color.GREEN); // Créer une bordure verte
            Border blueBevelBorder = BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.BLUE, Color.BLUE); // Créer un BevelBorder noir
            Border compoundBorder = BorderFactory.createCompoundBorder(blueBevelBorder, greenLineBorder); // Combinez les deux bordures
            Border greenBevelBorder = BorderFactory.createBevelBorder(BevelBorder.LOWERED, new Color(24, 64, 41), new Color(24, 64, 41)); // Créer un BevelBorder noir

            // Créer une bordure vide de couleur noire pour simuler l'ombre
            Border blackEmptyBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5); // Ajustez la largeur de l'ombre selon vos besoins

            // Créer une bordure composée en combinant la bordure biseautée avec la bordure vide noire
            Border compoun4dBorder = BorderFactory.createCompoundBorder(blueBevelBorder, blackEmptyBorder);

            // Appliquer la bordure composée à votre composant
            setBorder(compoundBorder);

            
            if(nbMiseAjour <= 1) {
                setBorder(compoundBorder);  // Condition pour la première bordure bleue
            } else {
                setBorder(compoun4dBorder); // Définir la bordure composée
            }
            
            repaint(); // Redessiner la vue de la carte
        }
    }


}
