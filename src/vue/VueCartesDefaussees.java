package vue;

import javax.swing.*;
import java.awt.*;
import model.*;
import java.util.ArrayList;
import java.util.List;

public class VueCartesDefaussees extends JPanel implements Observer {
    private List<Carte> cartesDefaussees;
    private Etat etat;

    public VueCartesDefaussees(Etat etat) {
        this.etat = etat;
        this.cartesDefaussees = etat.getCartesDefausses();

        etat.addObserver(this); // Observateur de l'état pour détecter les changements

        setBackground(new Color(180, 180, 180)); // Couleur de fond gris clair

        // Définir la taille préférée pour la vue des cartes défaussées
        setPreferredSize(new Dimension(200, 200));
    }

    @Override
    public void update(Observable o, Object arg) {
        // Mettre à jour la liste des cartes défaussées à partir de l'état
        this.cartesDefaussees = etat.getCartesDefausses(); // Redessiner la vue des cartes défaussées
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        removeAll();
        // Dessiner le rectangle principal avec fond noir gris
        g.setColor(Color.DARK_GRAY);
        g.fillRect(10, 10, getWidth() - 20, getHeight() - 20);

        // Dessiner les numéros de cartes à l'intérieur du rectangle
        int x = 20; // Position x de départ pour le premier numéro
        int y = 30; // Position y de départ pour le premier numéro
        int numPerRow = 6; // Nombre maximum de numéros par ligne
        int count = 0; // Compteur pour le nombre de numéros par ligne

        for (int i = 0; i < cartesDefaussees.size(); i++) {
            Carte carte = cartesDefaussees.get(i);
            String numeroCarte = Integer.toString(carte.getValeur());
            Color couleurCarte = getColorFromString(carte.getCouleur());

            // Définir la couleur pour le numéro de carte en fonction de la couleur de la carte
            g.setColor(couleurCarte);

            // Dessiner le numéro de carte à l'emplacement approprié
            // Récupérer les informations sur la police de caractères actuelle
            FontMetrics metrics = g.getFontMetrics();

            // Obtenir la hauteur de la police de caractères actuelle
            int fontHeight = metrics.getHeight();

            // Définir une nouvelle police avec une taille plus grande
            Font newFont = g.getFont().deriveFont(Font.BOLD, 20); // 20 est la nouvelle taille de la police

            // Définir la nouvelle police
            g.setFont(newFont);

            // Dessiner le numéro de carte avec la nouvelle police
            g.drawString(numeroCarte, x, y); 

            // Incrémenter la position x pour le prochain numéro
            x += 30;

            // Incrémenter le compteur pour le nombre de numéros par ligne
            count++;

            // Vérifier si nous devons passer à la ligne suivante
            if (count == numPerRow) {
                // Réinitialiser la position x à celle du début de la ligne
                x = 20;
                // Augmenter la position y pour passer à la ligne suivante
                y += 20;
                // Réinitialiser le compteur
                count = 0;
            }
        }
    }


    private Color getColorFromString(String couleur) {
        switch (couleur.toLowerCase()) {
            case "rouge":
                return Color.RED;
            case "jaune":
                return Color.YELLOW;
            case "bleu":
                return Color.BLUE;
            case "vert":
                return Color.GREEN;
            case "blanc":
                return Color.WHITE;
            default:
                return Color.BLACK;
        }
    }












}
