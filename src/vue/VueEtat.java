package vue;

import model.*;
import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.ArrayList; // Importez ArrayList

public class VueEtat extends JPanel {
    private Etat etat;
    private Joueur monjoueur; 
    private List<Joueur> autresjoueurs;
    private JButton jouerCarteButton;
    private JButton defausserCarteButton;
    private JButton donnerIndiceButton;
    private JButton utiliserIAButton;
    private CarteMouseListener carteMouseListener;


    public VueEtat(Etat etat, String nom_monjoueur) {
        this.etat = etat;
        carteMouseListener =new CarteMouseListener();
        autresjoueurs = new ArrayList<>(); // Utilisez ArrayList

        for (Joueur joueur : etat.getJoueurs()) {
            if (!joueur.getNom().equals(nom_monjoueur)) {
                autresjoueurs.add(joueur);
                
            } else {
                this.monjoueur = joueur;
                //joueur.montrermain();
            }
        }

        setBackground(new Color(24, 64, 41));


        setPreferredSize(new Dimension(1100, 800));
       // Désactiver le gestionnaire de mise en page par défaut
        setLayout(null);

        // Créer et ajouter le panneau pour le paquet de jeu en haut à gauche
        JPanel paquetPanel = createPaquetPanel();
        paquetPanel.setBounds(10, 10, 200, 100); // Position et taille du panneau
        add(paquetPanel);

        // Créer et ajouter les feux d'artifice au centre de la table
        JPanel feuxArtificePanel = createFeuxArtificePanel();
        feuxArtificePanel.setBounds(330, 150, 400, 250); // Position et taille du panneau
        add(feuxArtificePanel);

        // Créer et ajouter la main du joueur actuel en bas de la table
        JPanel mainMonJoueurPanel = createMainMonJoueurPanel();
        mainMonJoueurPanel.setBounds(400, 700, 300, 100); // Position et taille du panneau
        JPanel nompanel1 = new JPanel(new BorderLayout());
        nompanel1.setBounds(400 + 125 , 700-10, 50, 10); // Position et taille du panneau
        JLabel labelNomJoueur1 = new JLabel(monjoueur.getNom());
        labelNomJoueur1.setHorizontalAlignment(SwingConstants.CENTER); // Aligner le texte au centre horizontalement
        nompanel1.add(labelNomJoueur1,BorderLayout.CENTER);
        add(nompanel1);
        add(mainMonJoueurPanel);

        // Créer et ajouter la main des autres joueurs autour de la table
        int directionIndex = 0; // Index pour parcourir les directions cycliquement
        int[][] directions;
        if (autresjoueurs.size() <= 3) {
            directions = new int[][] {
                {400, 15}, // North
                {10, 300}, // West
                {750, 300} // East
            }; 
        } else {
            directions = new int[][] {
                {750, 150}, // East1
                {10, 150}, // west1
                {10, 350}, // West2
                {750, 350} // East2
            }; 
        }

        for (Joueur joueur : autresjoueurs) {
            // Créer un panneau pour chaque joueur
            JPanel joueurPanel = createMainAutreJoueurPanel(joueur);
            // Récupérer les coordonnées de la direction actuelle
            int[] direction = directions[directionIndex];

            JPanel nompanel = new JPanel(new BorderLayout());
            nompanel.setBounds(direction[0] + 125 , direction[1]-10, 50, 10); // Position et taille du panneau
            JLabel labelNomJoueur = new JLabel(joueur.getNom());
            labelNomJoueur.setHorizontalAlignment(SwingConstants.CENTER); // Aligner le texte au centre horizontalement
            nompanel.add(labelNomJoueur,BorderLayout.CENTER);


            // Ajouter le panneau du joueur à une direction différente selon son emplacement autour de la table
            joueurPanel.setBounds(direction[0], direction[1], 300, 100); // Position et taille du panneau
            add(joueurPanel);
            add(nompanel);
            // Incrémenter l'index de direction
            directionIndex = (directionIndex + 1) % directions.length;
        }


        // Créer et ajouter le panneau des jetons à l'EST de la vue d'état
        JPanel jetonsPanel = createJetonsPanel();
        jetonsPanel.setBounds(10, 650, 220, 100); // Position et taille du panneau
        add(jetonsPanel);

        JPanel defaussesPanel = createDefaussesPanel();
        defaussesPanel.setBounds(800, 600, 200, 200); // Position et taille du panneau
        add(defaussesPanel);


        //je veux ajouter 4 bouttons , Jouer Carte , Defausser Carte , Donner indice , et un boutton USE AI 
        // Créer et ajouter un bouton "Jouer Carte"
        jouerCarteButton = new JButton("Jouer Carte");
        jouerCarteButton.setBounds(10, 550, 150, 30); // Position et taille du bouton
        // Changez la couleur de fond du bouton jouerCarteButton
        jouerCarteButton.setBackground(Color.GRAY);
        add(jouerCarteButton);

        // Créer et ajouter un bouton "Défausser Carte"
        defausserCarteButton = new JButton("Défausser Carte");
        defausserCarteButton.setBounds(10, 450, 150, 30); // Position et taille du bouton
        defausserCarteButton.setBackground(Color.GRAY);
        add(defausserCarteButton);

        // Créer et ajouter un bouton "Donner Indice"
        donnerIndiceButton = new JButton("Donner Indice");
        donnerIndiceButton.setBounds(10, 500, 150, 30);
        donnerIndiceButton.setBackground(Color.GRAY);// Position et taille du bouton
        add(donnerIndiceButton);

        // Créer et ajouter un bouton "Utiliser IA"
        utiliserIAButton = new JButton("Utiliser IA");
        utiliserIAButton.setBounds(10, 600, 150, 30); // Position et taille du bouton
        utiliserIAButton.setBackground(Color.GRAY);
        add(utiliserIAButton);

    }

    private JPanel createPaquetPanel() {
        JPanel panel = new JPanel();

        VuePaquet vuePaquet = new VuePaquet(etat.getPaquet());
        panel.add(vuePaquet);
        panel.setBackground(new Color(24, 64, 41));

        return panel;
    }

    private JPanel createFeuxArtificePanel() {
        JPanel panel = new JPanel();
        VueFeuArtifice vueFeuArtifice = new VueFeuArtifice(etat.getFeux());
        panel.add(vueFeuArtifice);
        panel.setBackground(new Color(24, 64, 41));
        return panel;
    }

    private JPanel createMainMonJoueurPanel() {
        JPanel panel = new JPanel();
        VueMainMonJoueur vueMainMonJoueur = new VueMainMonJoueur(monjoueur,this.carteMouseListener); // Utilisez le nom de classe correct
        panel.add(vueMainMonJoueur);
        panel.setBackground(new Color(24, 64, 41));
        return panel;
    }

    private JPanel createMainAutreJoueurPanel(Joueur joueur) { // Ajoutez un paramètre Joueur
        JPanel panel = new JPanel();
        VueMainAutreJoueur vueMainAutreJoueur = new VueMainAutreJoueur(joueur,this.carteMouseListener); // Utilisez le nom de classe correct et le paramètre Joueur
        panel.add(vueMainAutreJoueur);
        panel.setBackground(new Color(24, 64, 41));
        return panel;
    }

    private JPanel createJetonsPanel1(int maxJetons, Color couleur) {
        JPanel panel = new JPanel();
        VueJetons vuejetons = new VueJetons(etat, maxJetons,couleur); 
        panel.add(vuejetons);
        panel.setBackground(new Color(24, 64, 41));
        // Ajouter le contenu nécessaire
        return panel;
    }

    private JPanel createJetonsPanel() {
        // Création du panneau pour les jetons
        JPanel jetonsPanel = new JPanel();
        jetonsPanel.setLayout(new BoxLayout(jetonsPanel, BoxLayout.Y_AXIS));

        // Création et ajout des jetons bleus
        JPanel jetonsBleusPanel = createJetonsPanel1(8, Color.BLUE);
        jetonsBleusPanel.setBorder(null); // Supprimer les marges internes du panneau de jetons bleus
        jetonsPanel.add(jetonsBleusPanel);

        // Création et ajout des jetons rouges
        JPanel jetonsRougesPanel = createJetonsPanel1(3, Color.RED);
        jetonsRougesPanel.setBorder(null); // Supprimer les marges internes du panneau de jetons rouges
        jetonsPanel.add(jetonsRougesPanel);

        jetonsPanel.setBackground(new Color(24, 64, 41));



        return jetonsPanel;
    }
    
    private JPanel createDefaussesPanel() {
        JPanel panel = new JPanel();
        VueCartesDefaussees vueCartesDefaussees = new VueCartesDefaussees(etat); 
        panel.add(vueCartesDefaussees);
        panel.setBackground(new Color(24, 64, 41));
        // Ajouter le contenu nécessaire
        return panel;
    }
    


    // Ajoutez des méthodes pour récupérer les boutons
    public JButton getJouerCarteButton() {
        return jouerCarteButton;
    }

    public JButton getDefausserCarteButton() {
        return defausserCarteButton;
    }

    public JButton getDonnerIndiceButton() {
        return donnerIndiceButton;
    }

    public JButton getUtiliserIAButton() {
        return utiliserIAButton;
    }

    public Etat getEtat(){
        return this.etat;
    }

    public void setMonJoueur(Joueur monjoueur){
        this.monjoueur = monjoueur;
    }

    public Joueur getMonJoueur(){
        return this.monjoueur;
    }

    public CarteMouseListener getCarteMouseListener(){
        return this.carteMouseListener;
    }
}
