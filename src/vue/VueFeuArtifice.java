package vue;

import javax.swing.*;
import java.awt.*;
import java.util.List; // Importer Observer
import model.*;

public class VueFeuArtifice extends JPanel implements Observer { // Implémenter Observer
    private FeuArtifice feuArtifice;
    private List<VueCarte> listevues;

    public VueFeuArtifice(FeuArtifice feuArtifice) {
        this.feuArtifice = feuArtifice;
        feuArtifice.addObserver(this);
        setPreferredSize(new Dimension(400, 350)); 
        setBackground(new Color(24, 64, 41));// Ajouter cette vue comme observateur de FeuArtifice
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        removeAll();
        // Initialiser les coordonnées de dessin
        int x = getWidth() / 2 - 150; // Position x de la première pile
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.ORANGE);
        g2d.setStroke(new BasicStroke(5));
        g2d.drawLine(x-10, getHeight() / 4 , x-10, getHeight()); // Dessiner une ligne entre les piles
        // Dessiner chaque pile de feux
        for (List<Carte> pile : feuArtifice.getPiles().values()) {
            int y = getHeight() / 2 -10 ; // Position y de la première carte de la pile
            for (int i = pile.size() - 1; i >= 0; i--) {
                Carte carte = pile.get(i);
                carte.revelerValeur();
                carte.revelerCouleur();
                VueCarte vueCarte = new VueCarte(carte,"Feu");
                vueCarte.setBounds(x, y, 50, 70); // Définir la taille de la carte
                add(vueCarte); // Ajouter la carte à la vue

                // Décaler la position y pour la prochaine carte
                y -= 20; // Petit décalage vertical entre les cartes dans la même pile
            }

            g2d.drawLine(x+60, getHeight() / 4 , x+60, getHeight());
                    // Dessiner une ligne orange entre les piles
            // Incrémenter la position x pour la prochaine pile
            x += 70; // Décaler la position x pour la prochaine pile
        } 
        g2d.setStroke(new BasicStroke(15));
        g2d.drawLine(45, 242 , 385, 242);
    }

    @Override
    public void update(Observable o, Object arg) {
        // Mettre à jour l'affichage lorsque l'état de FeuArtifice change
            repaint(); // Redessiner la vue
    }
}
