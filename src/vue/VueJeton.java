package vue;

import javax.swing.*;
import java.awt.*;

public class VueJeton extends JPanel {

    private Color couleur;

    public VueJeton(Color couleur) {
        this.couleur = couleur;
        setPreferredSize(new Dimension(20, 20));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(couleur);
        g.fillOval(0, 0, 30, 30);
    }
}