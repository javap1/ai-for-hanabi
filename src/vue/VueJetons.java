package vue;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import model.*;
import javax.swing.JPanel;

public class VueJetons extends JPanel implements Observer {

    private int jetonsPartie1;
    private int jetonsPartie2;
    private Etat etatObservable;
    private Color couleur;
    private int maxJetons;

    public VueJetons(Etat etatObservable, int maxJetons, Color couleur) {
        this.etatObservable=etatObservable;
        this.maxJetons = maxJetons;
        this.couleur = couleur;
        etatObservable.addObserver(this);
        mettreAJourJetons();
        setPreferredSize(new Dimension(220, 50));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        removeAll();
        int width = getWidth();
        int height = getHeight();

        // Dessiner la partie 1 avec un fond gris
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, width / 2, height);

        // Dessiner les jetons pour la première partie
        int y1 = height / 8;
        for (int i = 0; i < jetonsPartie1; i++) {
            int x = 10 + (i % 4) * 20; // Position horizontale du jeton
            int y = y1 + (i / 4) * 20; // Position verticale du jeton
            g.setColor(couleur);
            g.fillOval(x, y, 20, 20);
        }

        // Dessiner la partie 2 avec un fond gris
        g.setColor(Color.BLACK);
        g.fillRect(width / 2, 0, width / 2, height);

        // Dessiner les jetons pour la deuxième partie
        int y2 = height / 8;
        for (int i = 0; i < jetonsPartie2; i++) {
            int x = width / 2 + 10 + (i % 4) * 20; // Position horizontale du jeton
            int y = y2 + (i / 4) * 20; // Position verticale du jeton
            g.setColor(couleur);
            g.fillOval(x, y, 20, 20);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
            mettreAJourJetons();
            repaint();
    }

    private void mettreAJourJetons() {
        if (maxJetons == 8) {
            jetonsPartie1 = etatObservable.getJetonsBleu();
        } else if (maxJetons == 3) {
            jetonsPartie1 = etatObservable.getJetonsRouge();
        }
        jetonsPartie2 = maxJetons - jetonsPartie1;
    }
}
