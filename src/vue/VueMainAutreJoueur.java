package vue;

import java.util.Iterator; // Ajoutez cette importation
import model.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.BevelBorder;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class VueMainAutreJoueur extends JPanel implements Observer,VueJoueur  {
    private final Joueur joueur;
    private CarteMouseListener carteMouseListener;
    private List<Carte> main ;

    public VueMainAutreJoueur(Joueur joueur,CarteMouseListener carteMouseListener ) {
        this.joueur = joueur;
        this.carteMouseListener=carteMouseListener;
        setLayout(new FlowLayout(FlowLayout.LEFT)); // Définir un layout pour les cartes
        joueur.addObserver(this);
        afficherMain(); // Afficher la main initiale
    }
    @Override
    public void afficherMain() {
        main = new ArrayList<>(joueur.copyMain());
        
        for (Carte carte : main) {
            carte.revelerValeur();
            carte.revelerCouleur();
            VueCarte vueCarte = new VueCarte(carte,joueur.getNom()); // Créer une vue pour chaque carte
            vueCarte.addMouseListener(carteMouseListener);
            add(vueCarte); // Ajouter la vue de la carte au panneau de la main du joueur
        }
        revalidate(); // Mettre à jour l'affichage
        repaint(); // Redessiner la vue
    }

    public void MiseAjour() {
        Component[] components = getComponents(); // Récupérer les composants actuellement présents dans le JPanel

        // Supprimer les composants qui ne sont plus dans la main du joueur
        for (Component component : components) {
            if (component instanceof VueCarte) {
                VueCarte vueCarte = (VueCarte) component;
                if (!main.contains(vueCarte.getCarte())) {
                    remove(vueCarte);
                }
            }
        }

        // Ajouter les nouvelles cartes à la vue
        for (Carte carte : main) {
            boolean carteExistante = false;
            for (Component component : components) {
                if (component instanceof VueCarte) {
                    VueCarte vueCarte = (VueCarte) component;
                    if (vueCarte.getCarte().equals(carte)) {
                        carteExistante = true;
                        break;
                    }
                }
            }
            if (!carteExistante) {
                carte.revelerValeur();
                carte.revelerCouleur();
                VueCarte vueCarte = new VueCarte(carte, joueur.getNom()); // Créer une vue pour chaque carte
                vueCarte.addMouseListener(carteMouseListener);
                add(vueCarte); // Ajouter la vue de la carte au panneau de la main du joueur
            }
        }

        revalidate(); // Mettre à jour l'affichage
    }

    public void MiseAjourMain() {

        int lastIndex = joueur.getMain().size() - 1;
        //Faut traiter le cas ou  y a des carte doubles ou triples 
        // Supprimer les cartes de main qui ne sont pas dans la liste du joueur
        Iterator<Carte> iterator = main.iterator();
        while (iterator.hasNext()) {
            Carte carte = iterator.next();
            boolean carteExistante = false;
            for (int i=0;i<lastIndex ;i++) {
                Carte carteJoueur= joueur.getMain().get(i);
                if (carte.getValeur() == carteJoueur.getValeur() && carte.getCouleur().equals(carteJoueur.getCouleur())) {
                    carteExistante = true;
                    break;
                }
            }
            if (!carteExistante) {
                iterator.remove();
            }
        }

        //supprimer les doublons car on arrive pas à le detecter avec la fonction au dessus 
        if(main.size()!=joueur.getMain().size() - 1){  
            int carteASupprimer=-1;  
            for (int i = 0; i < main.size(); i++) {
                Carte carte = main.get(i);
                for (int j = i + 1; j < main.size(); j++) {
                    Carte carte2=main.get(j);
                    if (carte.getValeur() == carte2.getValeur() && carte.getCouleur().equals(carte2.getCouleur())) {
                        carteASupprimer=j;
                        break; // Sortir de la boucle interne une fois que la carte est supprimée
                    }
                }
                if(carteASupprimer!=-1)
                    break;
            }
            if(carteASupprimer!=-1)
                main.remove(carteASupprimer);
        }

        try {
            // Tentative d'ajout d'une copie de la dernière carte du joueur
            Carte carteACloner = joueur.getMain().get(lastIndex);
            Carte nouvelleCopy = (Carte) carteACloner.clone();
            main.add(nouvelleCopy);

        } catch (Exception e) {
            System.out.println("ERREUR : MainJoueur n'est pas assez grande ");
            // Gérer l'exception ici
        }

            
        

    }


    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Joueur) {
            MiseAjourMain();
            MiseAjour(); // Actualiser l'affichage de la main lorsque le joueur change
        }
    }
}
