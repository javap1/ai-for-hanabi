package vue;

import model.*;
import javax.swing.border.BevelBorder;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

public class VueMainMonJoueur extends JPanel implements Observer,VueJoueur {
    private final Joueur joueur;
    private CarteMouseListener carteMouseListener;


    public VueMainMonJoueur(Joueur joueur,CarteMouseListener carteMouseListener) {
        this.joueur = joueur;
        this.carteMouseListener =carteMouseListener;
        joueur.addObserver(this); // Ajouter cette vue comme observateur du joueur
        afficherMain(); // Afficher la main initiale
    }

    @Override
    public void afficherMain() {
        
        List<Carte> main = joueur.getMain();
        for (Carte carte : main) {
            VueCarte vueCarte = new VueCarte(carte,joueur.getNom()); // Créer une vue pour chaque carte
            vueCarte.addMouseListener(carteMouseListener);
            add(vueCarte); // Ajouter la vue de la carte au panneau de la main du joueur
        }
        revalidate(); // Mettre à jour l'affichage
        repaint(); // Redessiner la vue
    }

    public void MiseAjour() {
        List<Carte> main = joueur.getMain();
        Component[] components = getComponents(); // Récupérer les composants actuellement présents dans le JPanel

        // Supprimer les composants qui ne sont plus dans la main du joueur
        for (Component component : components) {
            if (component instanceof VueCarte) {
                VueCarte vueCarte = (VueCarte) component;
                if (!main.contains(vueCarte.getCarte())) {
                    remove(vueCarte);
                }
            }
        }

        // Ajouter les nouvelles cartes à la vue
        for (Carte carte : main) {
            boolean carteExistante = false;
            for (Component component : components) {
                if (component instanceof VueCarte) {
                    VueCarte vueCarte = (VueCarte) component;
                    if (vueCarte.getCarte().equals(carte)) {
                        carteExistante = true;
                        break;
                    }
                }
            }
            if (!carteExistante) {
                VueCarte vueCarte = new VueCarte(carte, joueur.getNom()); // Créer une vue pour chaque carte
                vueCarte.addMouseListener(carteMouseListener);
                add(vueCarte); // Ajouter la vue de la carte au panneau de la main du joueur
            }
        }

        revalidate(); // Mettre à jour l'affichage
        repaint(); // Redessiner la vue
    }


    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Joueur) {
            MiseAjour(); // Actualiser l'affichage de la main lorsque le joueur change
        }
    }

   
}
