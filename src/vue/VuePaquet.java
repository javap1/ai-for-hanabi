package vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;
import model.*;

public class VuePaquet extends JPanel implements Observer {
    private Paquet paquet;
    private int cardWidth = 50;
    private int cardHeight = 70;
    private int xOffset = 2; // Décalage horizontal réduit

    public VuePaquet(Paquet paquet) {
        this.paquet = paquet;
        this.paquet.addObserver(this); // Ajouter cette vue comme observateur du paquet
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        removeAll();
        // Dessiner le dos des cartes pour chaque carte dans le paquet
        for (int i = 0; i < paquet.taille() / 4; i++) {
            int xPos = i * xOffset;
            g.setColor(Color.BLACK);
            g.fillRect(xPos, 0, cardWidth, cardHeight);
            g.setColor(Color.WHITE); // Bordure blanche
            g.drawRect(xPos, 0, cardWidth, cardHeight);
        }

        // Dessiner le numéro de cartes à l'arrière de la dernière carte
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.PLAIN, 12));
        int taille = paquet.taille();
        int numX = (taille - 1) * xOffset + cardWidth / 2 - 5;
        int numY = cardHeight / 2 + 5;
        g.setFont(new Font("Arial", Font.PLAIN, 24));
        g.setColor(Color.GRAY); // Augmenter la taille de la police à 20
        g.drawString(String.valueOf(taille), numX, numY);
    }

    @Override
    public Dimension getPreferredSize() {
        // Taille du paquet
        int width = Math.min(paquet.taille() * xOffset + cardWidth, 300);
        return new Dimension(width, cardHeight);
    }

    @Override
    public void update(Observable o, Object arg) {
        // Mettre à jour la vue lorsque le paquet observable change
            repaint(); // Redessiner la vue
    }
}
