package vue;
import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.List;
import java.util.ArrayList;
import controleur.Controleur;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
 // Importez ArrayList


import model.*;

public class VuePrincipale {
    private final List<Joueur> joueurs;
    private Etat etat;
    private int score;
    private JFrame frame; // Déclaration de frame comme attribut de classe
    


    public static void main(String[] args) {
        //une fenetre pour demander le nombre de joueurs
        // Options de choix pour le nombre de joueurs
        Integer[] options = {2, 3, 4,5};
        
        // Panneau contenant la liste déroulante
        JPanel panel = new JPanel();
        JComboBox<Integer> comboBox = new JComboBox<>(options);
        panel.add(comboBox);
        
        // Afficher la boîte de dialogue avec la liste déroulante
        int result = JOptionPane.showOptionDialog(null, panel, "Choisissez le nombre de joueurs", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, options[0]);
        
        if (result == JOptionPane.CLOSED_OPTION) {
            // L'utilisateur a fermé la boîte de dialogue sans sélectionner d'option
            // Quitter l'application
            System.exit(0);
        }
        
        // L'utilisateur a sélectionné une option et a appuyé sur OK
        int nombreJoueurs = (int) comboBox.getSelectedItem();
        
        // Créer une instance de la vue principale avec le nombre de joueurs sélectionné
        VuePrincipale vuePrincipale = new VuePrincipale(nombreJoueurs);
        vuePrincipale.initialize();
    
    }


    public VuePrincipale(int nbjoueurs) {
        joueurs = new ArrayList<>();
        // Initialisation de la liste des joueurs
        
        // Create players
        for (int i = 1; i <= nbjoueurs; i++) {
            joueurs.add(new Joueur("" + i));
        }
        // Création de l'état avec les joueurs
        this.etat = new Etat(joueurs);
        this.score = 0;
    }
    
    public void initialize() {
        frame = new JFrame("Vue Principale");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        VueEtat vueEtat = new VueEtat(etat, ""+1);
        Controleur controleur = new Controleur(vueEtat);
        
        // Ajout de la vue d'état à la fenêtre principale
        frame.add(vueEtat);
        frame.pack();
        frame.setVisible(true);
    }







}
